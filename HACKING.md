# Important information to know when working on Vicinitude

## The Vicinitude Config File
/config/vicinitude.yml is the central configuration file for the Vicinitude
web application. Even when some config data is kept in the databse and entry
in this file will point to the specific row of a table. For example the
"map_source" entry indicates which item of the MapSource model is to be used.

If add a new entry into this file YOU MUST edit the "import_config" action of
the Console controller to load your new entry.

## "Special" Models
The MapSource models is special in that is has a fixed number of entries. The
first row id used for admin provided (custom) data and every subsequent row is
provided via the /db/seeds.rb file.

There used to be a Theme model that followed this rule as well.

## Style

### Spelling
Whenever possible function and variable names should be spelt in Standard
English, not American English. For example "Centre" rather than "Center".
The exceptions are when dependencies such as the Document Object Model
already use an American spellings like "Color".

### Indentation
Every source file should end with a note containing the correct Vim settings.
If you do not use Vim (or something compatible) please set your editor to
insert tabs (not spaces) and set your tab-width to four characters.

### Variable and Function Names
This comes down to which language you are writing in. Ruby code should use
"snake case" (like_this) whereas JS code should use "camel case" (likeThis).
If you find any exceptions please make a pull request on GitLab.
