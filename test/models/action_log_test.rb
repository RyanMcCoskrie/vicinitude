require "test_helper"

class ActionLogTest < ActiveSupport::TestCase
	def setup
		@log = ActionLog.create(json: {
			user_name: 'test', user_id: 1,
			model: 'event', action: 'create',
			entry_id: '1', entry_name: 'picnic'
		})
	end

	test "can save valid action log" do
		assert @log.save
	end

	test "can read valid action log as JSON" do
		assert @log.json
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
