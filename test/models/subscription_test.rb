require "test_helper"

class SubscriptionTest < ActiveSupport::TestCase
	fixtures :users, :locations, :organisations

	def setup
		@location = locations(:one)
		@location.save
		@user = users(:one)
		@user.save
		@org = organisations(:one)
		@org.save

		@sub1 = subscriptions(:one)
		@sub2 = subscriptions(:two)
	end

	test "can save a valid subscription" do
		assert @sub1.save
		assert @sub2.save
	end

	test "can not save a Subscription without a user" do
		@sub1.user = nil
		assert_not @sub1.save
	end

	test "can not save a Subscription without a subscribable" do
		@sub1.subscribable = nil
		assert_not @sub1.save
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
