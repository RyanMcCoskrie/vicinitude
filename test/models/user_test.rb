require "test_helper"

class UserTest < ActiveSupport::TestCase
	fixtures :locations
	def setup
		@location = locations(:one)
		@location.save
	end

	test "can save valid user" do
		user = User.new
		user.user_name = "fake"
		user.real_name = "Fake User"
		user.email = "fake-user@example.net"
		user.password = "phraseforfakeuser"
		user.home_location = @location
		user.priv = 0

		assert user.save
	end

	test "can't save user without user name" do
		user = User.new
		user.real_name = "Fake User"
		user.email = "fake-user@example.net"
		user.password = "phraseforfakeuser"
		user.home_location = @location
		user.priv = 0

		assert_not user.save
	end

	test "can't save user without real name" do
		user = User.new
		user.user_name = "fake"
		user.email = "fake-user@example.net"
		user.password = "phraseforfakeuser"
		user.home_location = @location
		user.priv = 0

		assert_not user.save
	end

	test "can't save user without email" do
		user = User.new
		user.user_name = "fake"
		user.real_name = "Fake User"
		user.password = "phraseforfakeuser"
		user.home_location = @location
		user.priv = 0

		assert_not user.save
	end

	test "can't save user without password" do
		user = User.new
		user.user_name = "fake"
		user.real_name = "Fake User"
		user.email = "fake-user@example.net"
		user.home_location = @location
		user.priv = 0

		assert_not user.save
	end

	test "can't save user without privileges" do
		user = User.new
		user.user_name = "fake"
		user.real_name = "Fake User"
		user.email = "fake-user@example.net"
		user.password = "phraseforfakeuser"
		user.home_location = @location

		assert_not user.save
	end

	test "passwords work" do
		user = User.new
		user.password = "phraseforfakeuser"

		assert user.authenticate("phraseforfakeuser")
	end

	test "passwords are hashed" do
		user = User.new
		user.password = "phraseforfakeuser"

		assert_not user.password_hash == "phraseforfakeuser"
	end

	test "passwords are re-salted" do
		user = User.new
		user.password = "phraseforfakeuser"
		salt_one = user.password_salt
		user.password = "newphrase"
		salt_two = user.password_salt

		assert_not salt_one == salt_two
	end

	test "privileges work" do
		user = User.new
		user.priv = 15

		assert user.verified?
		assert user.suspended?
		assert user.moderator?
		assert user.admin?

		user.priv = 0
		assert_not user.verified?
		assert_not user.suspended?
		assert_not user.moderator?
		assert_not user.admin?
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
