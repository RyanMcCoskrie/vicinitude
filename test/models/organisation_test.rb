require "test_helper"

class OrganisationTest < ActiveSupport::TestCase
	def setup
		@location = Location.new
		@location.name = "Nowhere"
		@location.lat = 4.242
		@location.lng = -42.42

		@user = User.new
		@user.user_name = "fake"
		@user.real_name = "Fake User"
		@user.email = "fake-user@example.net"
		@user.password = "phraseforfakeuser"
		@user.home_location = @location
		@user.priv = 0
	end

	test "Can create a valid organisation" do
		club = Club.new
		club.name = "Computer Club"
		club.public = true
		club.descr = "Club for programmers, admins and other computer nerds"
		club.registrant = @user
		club.location = @location

		assert club.save
	end

	test "Registrant is added to members" do
		club = Club.new
		club.name = "Computer Club"
		club.public = true
		club.descr = "Club for programmers, admins and other computer nerds"
		club.registrant = @user
		club.location = @location
		club.save

		assert club.member_ids.include? @user.id
	end

	test "Cant create an organisation without a name" do
		club = Club.new
		club.public = true
		club.descr = "Club for programmers, admins and other computer nerds"
		club.registrant = @user
		club.location = @location

		assert_not club.save
	end

	test "Cant create an organisation without a registrant" do
		club = Club.new
		club.public = true
		club.name = "Computer Club"
		club.descr = "Club for programmers, admins and other computer nerds"
		club.location = @location

		assert_not club.save
	end

	test "Cant create an organisation without a location" do
		club = Club.new
		club.public = true
		club.name = "Computer Club"
		club.descr = "Club for programmers, admins and other computer nerds"
		club.registrant = @user

		assert_not club.save
	end

	test "Can't create an organisation without a public field" do
		club = Club.new
		club.name = "Computer Club"
		club.descr = "Club for programmers, admins and other computer nerds"
		club.registrant = @user
		club.location = @location

		assert_not club.save
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
