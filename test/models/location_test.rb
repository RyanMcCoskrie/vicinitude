require "test_helper"

class LocationTest < ActiveSupport::TestCase
	test "can save a valid root location" do
		location = Location.new
		location.name = "Nowhere"
		location.lat = 4.242
		location.lng = -42.42

		assert location.save
	end

	test "can save a valid sub-location" do
		locationA = Location.create(name: "Nowhere", lat: 4.242, lng: -42.42)

		locationB = Location.new
		locationB.name = "Nowhere square"
		locationB.parent = locationA
		locationB.lat = 4.24242
		locationB.lng = -42.4242

		assert locationB.save
	end

	test "can't save a location without a name" do
		location = Location.new
		location.lat = 4.242
		location.lng = -42.42

		assert_not location.save
	end

	test "can't save a location without a lattitude" do
		location = Location.new
		location.name = "Nowhere"
		location.lng = -42.42

		assert_not location.save
	end

	test "can't save a location without a longitude" do
		location = Location.new
		location.name = "Nowhere"
		location.lat = 4.242

		assert_not location.save
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
