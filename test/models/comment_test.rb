require "test_helper"

class CommentTest < ActiveSupport::TestCase
	fixtures :users, :locations, :events, :comments

	def setup
		@location = locations(:one)
		@location.save
		@user = users(:one)
		@user.save
		@event = events(:one)
		@event.save
	end

	test "can save a valid comment" do
		comment = Comment.new
		comment.commentable = @event
		comment.user = @user
		comment.time = "30-12-2022 12:00"
		comment.body = "I'll bring organge juice"

		assert comment.save
	end

	test "can't save a comment without commentable" do
		comment = Comment.new
		comment.user = @user
		comment.time = "30-12-2022 12:00"
		comment.body = "I'll bring orange juice"

		assert_not comment.save
	end

	test "can't save a comment without user" do
		comment = Comment.new
		comment.commentable = @event
		comment.time = "30-12-2022 12:00"
		comment.body = "I'll bring organge juice"

		assert_not comment.save
	end

	test "can't save a comment without time" do
		comment = Comment.new
		comment.commentable = @event
		comment.user = @user
		comment.body = "I'll bring organge juice"

		assert_not comment.save
	end

	test "can't save a comment with invalid time" do
		comment = Comment.new
		comment.commentable = @event
		comment.user = @user
		comment.time = "Noon, second to last day of the year"
		comment.body = "I'll bring organge juice"

		assert_not comment.save
	end

	test "can't save a comment without body" do
		comment = Comment.new
		comment.commentable = @event
		comment.user = @user
		comment.time = "30-12-2022 12:00"

		assert_not comment.save
	end

	test "comment deleted with commentable" do
		comment = Comment.new
		comment.commentable = @event
		comment.user = @user
		comment.time = "30-12-2022 12:00"
		comment.body = "I'll bring organge juice"

		@user.save
		@event.save
		comment.save
		@event.destroy

		assert_not Comment.find_by_body("I'll bring organge juice")
	end

	test "comment deleted with user" do
		comment = Comment.new
		comment.commentable = @event
		comment.user = @user
		comment.time = "30-12-2022 12:00"
		comment.body = "I'll bring organge juice"

		@user.save
		@event.save
		comment.save
		@user.destroy

		assert_not Comment.find_by_body("I'll bring organge juice")
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
