require "test_helper"

class MapSourceTest < ActiveSupport::TestCase
	test "can save valid map source" do
		source = MapSource.new
		source.name = "Made up"
		source.url = "https://www.example.com/made-up.json"
		source.is_vector = true
		source.attribution = "Me, Myself and I"

		assert source.save
	end

	test "can save map source without attribution" do
		source = MapSource.new
		source.name = "Made up"
		source.url = "https://www.example.com/made-up.json"
		source.is_vector = true

		assert source.save
	end

	test "can not save map source without name" do
		source = MapSource.new
		source.url = "https://www.example.com/made-up.json"
		source.is_vector = true
		source.attribution = "Me, Myself and I"

		assert_not source.save
	end

	test "can not save map source without url" do
		source = MapSource.new
		source.name = "Made up"
		source.is_vector = true
		source.attribution = "Me, Myself and I"

		assert_not source.save
	end

	test "can not save map source without is_vector" do
		source = MapSource.new
		source.name = "Made up"
		source.url = "https://www.example.com/made-up.json"
		source.attribution = "Me, Myself and I"

		assert_not source.save
	end

end

# vim: ts=4 sts=4 sw=0 noexpandtab
