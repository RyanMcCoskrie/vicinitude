require "test_helper"

class EventTest < ActiveSupport::TestCase
	fixtures :users, :locations, :organisations, :events
	def setup
		@location = locations(:one)
		@location.save
		@user = users(:two)
		@user.save
		@org = organisations(:one)
		@org.save
	end

	test "can save a valid event" do
		#Without org
		assert events(:one).save

		#With org
		assert events(:two).save
	end

	test "can't save an event without a name" do
		event = Event.new
		event.start_time = "01-01-2023 12:00"
		event.end_time = "01-01-2023 14:00"
		event.location = @location
		event.user = @user

		assert_not event.save
	end

	test "can't save an event without a start time" do
		event = Event.new
		event.name = "Picnic"
		event.end_time = "01-01-2023 14:00"
		event.location = @location
		event.user = @user

		assert_not event.save
	end

	test "can't save an event without an end time" do
		event = Event.new
		event.name = "Picnic"
		event.start_time = "01-01-2023 12:00"
		event.location = @location
		event.user = @userd

		assert_not event.save
	end

	test "can't save an event without a location" do
		event = Event.new
		event.name = "Picnic"
		event.start_time = "01-01-2023 12:00"
		event.end_time = "01-01-2023 14:00"
		event.user = @user

		assert_not event.save
	end

	test "can't save an event without an user" do
		event = Event.new
		event.name = "Picnic"
		event.start_time = "01-01-2023 12:00"
		event.end_time = "01-01-2023 14:00"
		event.location = @location

		assert_not event.save
	end

	test "can't save an event with an invalid start time" do
		event = Event.new
		event.name = "Picnic"
		event.start_time = "Midday, first day of twenty-twenty-three"
		event.end_time = "01-01-2023 14:00"
		event.location = @location
		event.user = @user

		assert_not event.save
	end

	test "can't save an event with an invalid end time" do
		event = Event.new
		event.name = "Picnic"
		event.start_time = "01-01-2023 14:00"
		event.end_time = "Two o'clock, first of January, 2023"
		event.location = @location
		event.user = @user

		assert_not event.save
	end

	test "event is deleted when location is destroyed" do
		event = Event.new
		event.name = "Picnic"
		event.start_time = "01-01-2023 12:00"
		event.end_time = "01-01-2023 14:00"
		event.location = @location
		event.user = @user

		@location.save
		event.save
		@location.destroy

		assert_not Event.find_by_name("Picnic")
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
