require "test_helper"

class DiscussionTest < ActiveSupport::TestCase
	fixtures :users, :locations, :organisations, :discussions

	def setup
		@user = users(:five)
		@user.save
		@organisation = organisations(:one)
		@organisation.save
		@discussion = discussions(:one)
	end

	test "can save a valid discussion" do
		assert @discussion.save
	end

	test "can't save a discussion without a poster" do
		@discussion.poster = nil
		assert_not @discussion.save
	end

	test "can't save a discussion without an organisation" do
		@discussion.organisation = nil
		assert_not @discussion.save
	end

	test "can't save a discussion without a valid time" do
		@discussion.time = nil
		assert_not @discussion.save
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
