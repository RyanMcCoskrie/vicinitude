require "test_helper"

class ConsoleControllerTest < ActionDispatch::IntegrationTest
	fixtures :users, :locations

	test "should reject logged out users" do
		get console_root_path
		assert_response :redirect
		get console_users_path
		assert_response :redirect
		get new_console_user_path
		assert_response :redirect
		get console_locations_path
		assert_response :redirect
		get new_console_location_path
		assert_response :redirect
		get console_events_path
		assert_response :redirect
		get new_console_event_path
		assert_response :redirect
		get console_logs_path(24)
		assert_response :redirect
		get console_announcement_path
		assert_response :redirect
	end

	test "should reject non-admin users" do
		post authenticate_path, params: {
			user_name: "one",
			password: "12345678"
		}

		get console_root_path
		assert_response :redirect
		get console_users_path
		assert_response :redirect
		get new_console_user_path
		assert_response :redirect
		get console_locations_path
		assert_response :redirect
		get new_console_location_path
		assert_response :redirect
		get console_organisations_path
		assert_response :redirect
		get new_console_organisation_path
		assert_response :redirect
		get console_events_path
		assert_response :redirect
		get new_console_event_path
		assert_response :redirect
		get console_logs_path(24)
		assert_response :redirect
		get console_announcement_path
		assert_response :redirect
	end

	test "should accept admin users" do
		post authenticate_path, params: {
			user_name: "admin",
			password: "12345678"
		}

		get console_root_path
		assert_response :success
		get console_users_path
		assert_response :success
		get new_console_user_path
		assert_response :success
		get console_locations_path
		assert_response :success
		get new_console_location_path
		assert_response :success
		get console_organisations_path
		assert_response :success
		get new_console_organisation_path
		assert_response :success
		get console_events_path
		assert_response :success
		get new_console_event_path
		assert_response :success
		get console_logs_path(24)
		assert_response :success
		get console_announcement_path
		assert_response :success
	end

	test "should accept bulk location operations" do
		post authenticate_path, params: {
			user_name: "admin",
			password: "12345678"
		}

		get to_csv_console_locations_path
		assert_response :success

		post from_csv_console_locations_path, params:
			{file: fixture_file_upload('test/fixtures/files/locations.csv')}
		assert_response :redirect
	end

	test "should accept bulk config operations" do
		post authenticate_path, params: {
			user_name: "admin",
			password: "12345678"
		}

		get console_config_export_path
		assert_response :success

		post console_config_import_path, params:
			{file: fixture_file_upload('test/config.yml')}
	end

	test "should create, get, update and delete organisations" do
		locations(:one).save
		post authenticate_path, params: {
			user_name: "admin",
			password: "12345678"
		}

		post console_organisations_path, params: { organisation: {
			type: "Club",
			name: "Book Club",
			registrant_id: User.find_by(user_name: "admin").id,
			public: false,
			descr: "The most exciting Friday night in Town",
			location_id: locations(:one).id
		}}

		assert_response :redirect
		follow_redirect!
		assert_response :success
		assert_not_nil flash[:success]
		assert_nil flash[:error]

		id = Organisation.last.id

		get console_organisation_path(id)
		assert_response :success

		patch console_organisation_path(id), params: { organisation: {
				type: "Club",
				name: "Book Club",
				registrant_id: User.find_by(user_name: "admin").id,
				public: true,
				descr: "The most exciting Friday night in Town",
				location_id: locations(:one).id
		}}
		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]

		delete console_organisation_path(id)
		assert_response :redirect
		get console_organisation_path(id)
		assert_response :redirect
		assert_not_nil flash[:error]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
