require "test_helper"

class EventControllerTest < ActionDispatch::IntegrationTest
	fixtures :users, :locations, :organisations, :events, :comments

	def setup
		@user = users(:one)
		@user.save
		@org1 = organisations(:one)
		@org1.save
		@org2 = organisations(:two)
		@org2.save
		@loc1 = locations(:one)
		@loc1.save
		@event = events(:one)
		@event.save
	end

	test "should redirect unless logged in" do
		get events_path
		assert_response :redirect

		post authenticate_path, params: {user_name: "one", password: "12345678"}
		
		get events_path
		assert_response :success
	end

	test "should post, patch, get & delete valid events" do
		post authenticate_path, params: {user_name: "five", password: "12345678"}

		start_time = DateTime.now.next_day.at_noon
		end_time = start_time + 2.hours
        post events_path, params: {
			event: {
				"name": "Picnic",
				"descr": "Bring a plate to share",
				"location_id": @loc1.id,
				"organisation_id": "",
				"start_time": start_time.to_fs('%F %R'),
				"end_time": end_time.to_fs('%F %R')
			}
        }

		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]

		get event_path(Event.last.id)
		assert_response :success

		patch event_path(Event.last.id), params: {
			event: {
				"name": "Picnic",
				"descr": "Bring a plate to share",
				"location_id": @loc1.id.to_s,
				"organisation_id": "",
				"start_time": start_time.to_fs('%F %R'),
				"end_time": end_time.to_fs('%F %R')
			}
        }

		assert_response :redirect
		assert_nil flash[:error]
		assert_not_nil flash[:success]
	end

	test "should post & patch events with organisations" do
		post authenticate_path, params: {user_name: "five", password: "12345678"}

		start_time = DateTime.now.next_day.at_noon
		end_time = start_time + 2.hours
		post events_path, params: {
			event: {
				"name": "Picnic",
				"descr": "Bring a plate to share",
				"location_id": @loc1.id.to_s,
				"organisation_id": @org1.id.to_s,
				"start_time": start_time.to_fs('%F %R'),
				"end_time": end_time.to_fs('%F %R')
			}
        }

		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]

		patch event_path(Event.last.id), params: {
			event: {
				"name": "Picnic",
				"descr": "Bring a plate to share",
				"location_id": @loc1.id.to_s,
				"organisation_id": @org2.id.to_s,
				"start_time": start_time.to_fs('%F %R'),
				"end_time": end_time.to_fs('%F %R')
			}
        }

		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "should post comments to events" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		post event_comments_path(Event.first), params: {
			comment: {
				body: "I'll bring orange juice"
			}
		}
		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "should delete extant comments from events" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		delete event_comment_path(Event.first, Comment.first)
		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
