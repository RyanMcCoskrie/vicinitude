require "test_helper"

class LocationControllerTest < ActionDispatch::IntegrationTest
	fixtures :locations, :users, :subscriptions

	def setup
		locations(:one).save
		users(:one).save
		users(:two).save
		subscriptions(:two).save
	end

	test "should redirect unless logged in" do
		get locations_path
		assert_response :redirect

		post authenticate_path, params: {user_name: "one", password: "12345678"}
		
		get locations_path
		assert_response :success
	end

	test "should get valid locations" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}
		
		get location_path(Location.first.name)
		assert_response :success
	end

	test "should be able to subscribe locations" do
		subCount = Subscription.count
		post authenticate_path, params: {user_name: "admin", password: "12345678"}

		get location_subscribe_path(Location.first.name)
		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]
		assert_equal Subscription.count, subCount + 1
	end

	test "should be able to unsubscribe from locations" do
		subCount = Subscription.count
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get location_unsubscribe_path(Location.first.name)
		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]
		assert_equal Subscription.count, subCount - 1
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
