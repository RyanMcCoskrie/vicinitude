require "test_helper"

class FeedbackControllerTest < ActionDispatch::IntegrationTest
	def setup
		post authenticate_path, params: {
			user_name: "one",
			password: "12345678"
		}
	end

	test "pages work" do
		get feedback_path
		assert_response :success
		get feedback_general_path
		assert_response :success
		get feedback_missing_location_path
		assert_response :success
	end

	test "submissions work" do
		post feedback_general_path, params: {
			subject: "Test message",
			body: "Please ignore"
		}
		assert_response :redirect
		follow_redirect!

		assert_response :success
		assert_equal home_path, path
		assert_equal "Your message has been sent", flash[:success]
		assert_nil flash[:error]

		assert_response :success
		assert_equal home_path, path
		assert_equal "Your message has been sent", flash[:success]
		assert_nil flash[:error]

		post feedback_missing_location_path, params: {
			name: "Nowhereville town square",
			body: "Please ignore"
		}
		assert_response :redirect
		follow_redirect!

		assert_response :success
		assert_equal home_path, path
		assert_equal "Your message has been sent", flash[:success]
		assert_nil flash[:error]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
