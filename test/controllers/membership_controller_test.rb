require "test_helper"

class MembershipControllerTest < ActionDispatch::IntegrationTest
	fixtures :users, :locations

	#TODO Figure out how to use Organisation fixtures without foreign
	#key violations
	def setup
		users(:one).save
		users(:two).save
		locations(:one).save
		organisations(:one).save
		membership = memberships(:two)
		@sgid = membership.signed_id expires_in: 7.days
		membership.save
	end

	test "registrants can accept applicants" do
		get membership_accept_application_path(@sgid)
		membership = Membership.find_by(
			organisation: organisations(:one),
			member: users(:two)
		)

		assert_response :success
		assert_not_nil flash[:success]
		assert_nil flash[:error]
		assert_equal 2, membership.priv
	end

	test "registrants can reject applicants" do
		get membership_reject_application_path(@sgid)

		assert_response :success
		assert_not_nil flash[:success]
		assert_nil flash[:error]
		assert_nil Membership.find_by(organisation: organisations(:one),
				member: User.second)
	end

	test "invitees can accept invitations" do
		@org_sgid = organisations(:one).signed_id(expires_in: 7.days)
		@user_sgid = users(:one).signed_id(expires_in: 7.days)

		get membership_accept_invitation_path(@org_sgid, @user_sgid)
		assert_response :success
		assert_nil flash[:error]
		assert_not_nil flash[:success]
	end

	test "invitees can reject invitations" do
		@org_sgid = organisations(:one).signed_id(expires_in: 7.days)
		@user_sgid = users(:one).signed_id(expires_in: 7.days)

		get membership_accept_invitation_path(@org_sgid, @user_sgid)
		assert_response :success
		assert_nil flash[:error]
		assert_not_nil flash[:success]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
