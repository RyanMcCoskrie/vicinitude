require "test_helper"

class DiscussionControllerTest < ActionDispatch::IntegrationTest
	fixtures :locations, :users, :organisations, :discussions, :comments

	def setup
		locations(:one).save
		users(:one).save
		users(:two).save
		users(:five).save
		@org = organisations(:one)
		@org.save
		@discussion = discussions(:one)
		@discussion.save
		@comment = comments(:two)
		@comment.save
		comments(:three).save
	end

	test "should redirect unless logged in" do
		get organisation_discussions_path(organisation_name: @org.name)
		assert_response :redirect

		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get organisation_discussions_path(organisation_name: @org.name)
		assert_response :success
	end

	test "should get valid discussions" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get organisation_discussion_path(
			organisation_name: @org.name,
			id: @discussion.id
		)
		assert_response :success
	end

	test "Can post a valid discussion with first comment" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		post organisation_discussions_path(@org.name), params: {
			discussion: {
				name: "Saturday night chit-chat",
			},
			comment: {
				body: "How are you all?"
			}
		}
		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "Can post further comments to discussion" do
		post authenticate_path, params: {user_name: "admin", password: "12345678"}

		post organisation_discussion_comments_path(@org.name, @discussion.id),
		params: {
			comment:{
				body: "Enjoying the sunny weather"
			}
		}

		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "Can delete individual comments from discussion" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		delete organisation_discussion_comment_path(
			@org.name,
			@discussion.id,
			comments(:three).id
		)

		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "Users with moderation priviliges can delete any discussion" do
		post authenticate_path, params: {user_name: "admin", password: "12345678"}

		delete organisation_discussion_path(@org.name, @discussion)
		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "Users can delete the conversations they have posted" do
		post authenticate_path, params: {user_name: "five", password: "12345678"}

		delete organisation_discussion_path(@org.name, @discussion)
		assert_response :redirect
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "Random users can not delete discussions" do
		post authenticate_path, params: {user_name: "two", password: "12345678"}

		delete organisation_discussion_path(@org.name, @discussion)
		assert_response :redirect
		assert_not_nil flash[:error]
		assert_nil flash[:success]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
