require "test_helper"

class HomeControllerTest < ActionDispatch::IntegrationTest
	#######################################################################
	# IMPORTANT                                                           #
	#                                                                     #
	# Testing of /home handled by SessionControllerTest along with /login #
	#######################################################################

	fixtures :locations
	def setup
		locations(:one).save

		post authenticate_path, params: {
			user_name: "one",
			password: "12345678"
		}
	end

	test "pages work" do
		get profile_path
		assert_response :success
		get profile_edit_path
		assert_response :success
	end

	test "PATCH /profile works with home location" do
		patch profile_path, params: {
			real_name: "First Test User",
			email: "one@something.com",
			password: "87654321",
			confirmation: "87654321",
			home_location_id: locations(:one).id
		}
		assert_response :redirect
		follow_redirect!

		assert_equal profile_path, path
		assert_nil flash[:error]
		assert_equal "Profile updated", flash[:success]
	end

	test "PATCH /profile works without home location" do
		patch profile_path, params: {
			real_name: "First Test User",
			email: "one@something.com",
			password: "87654321",
			confirmation: "87654321"
		}
		assert_response :redirect
		follow_redirect!

		assert_equal profile_path, path
		assert_nil flash[:error]
		assert_equal "Profile updated", flash[:success]
	end


	test "can request verification" do
		post profile_request_verification_path, params: {
			address: "123 Fake rd\nNowhereville"
		}
		follow_redirect!

		assert_equal profile_path, path
		assert_nil flash[:error]
		assert_equal "Request sent", flash[:success]
	end

	test "can confirm verification" do
		get logout_path
		post authenticate_path, params: {
			user_name: "four",
			password: "12345678"
		}

		post profile_confirm_verification_path, params: {
			code: "5d8319d8"
		}
		assert_response :redirect
		follow_redirect!

		assert_equal profile_path, path
		assert_nil flash[:error]
		assert_equal "You are now verified!", flash[:success]
	end

	test "gives already verified error" do
		post profile_confirm_verification_path, params: {
			verification_code: "5d8319d8"
		}
		follow_redirect!

		assert_equal profile_path, path
		assert_nil flash[:success]
		assert_equal "You are already verified", flash[:error]

	end

	test "can delete profile" do
		delete profile_path
		follow_redirect!

		assert_equal root_path, path
		assert_equal "Your account has been deleted", flash[:success]
		assert_nil flash[:error]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
