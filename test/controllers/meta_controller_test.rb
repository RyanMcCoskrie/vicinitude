require "test_helper"

class MetaControllerTest < ActionDispatch::IntegrationTest
	test "should get index" do
		get root_path
		assert_response :success
	end

	test "should redirect if logged in" do
		post authenticate_path, params: {
			user_name: "one",
			password: "12345678"
		}

		get root_path
		assert_response :redirect

		follow_redirect!
		assert_equal home_path, path
	end

	test "should get about" do
		get about_path
		assert_response :success
	end

	test "should get contact" do
		get contact_path
		assert_response :success
	end

	test "should get terms" do
		get terms_path
		assert_response :success
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
