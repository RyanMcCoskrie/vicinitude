require "test_helper"

class SessionControllerTest < ActionDispatch::IntegrationTest
	fixtures :users, :locations

	def setup
		locations(:one).save
	end

	test "should get login" do
		get login_path
		assert_response :success
	end

	test "should login successfully" do
		#TODO Figure out how to put passwords into the fixture set
		post authenticate_path, params: {user_name: "one", password: "12345678"}
		follow_redirect!

		assert_response :success
		assert_equal home_path, path

		assert_nil flash[:error]
		assert_equal "Successfully logged in", flash[:success]
	end

	test "should reject bad passwords" do
		post authenticate_path, params: {user_name: "one", password: "87654321"}

		assert_response :forbidden
		assert_equal "Invalid user name or password", flash[:error]
		assert_nil flash[:success]
	end

	test "should get signup" do
		get signup_path
		assert_response :success
	end

	test "should register new user with home location" do
		post register_path, params: {
			user_name: "fred",
			real_name: "Fred Foobar",
			email: "fred@foobar.net",
			password: "12345678",
			confirmation: "12345678",
			home_location_id: locations(:one).id
		}
		follow_redirect!

		assert_response :success
		assert_equal home_path, path

		assert_equal "Account created", flash[:success]
		assert_nil flash[:error]
	end

	test "should register new user without home location" do
		post register_path, params: {
			user_name: "fred",
			real_name: "Fred Foobar",
			email: "fred@foobar.net",
			password: "12345678",
			confirmation: "12345678",
			home_location_id: ""
		}
		follow_redirect!

		assert_response :success
		assert_equal home_path, path

		assert_equal "Account created", flash[:success]
		assert_nil flash[:error]
	end

	test "should reject duplicate user name" do
		post register_path, params: {
			user_name: "one",
			real_name: "Test User",
			email: "test@example.com",
			password: "12345678",
			confirmation: "12345678",
			home_location_id: locations(:one).id
		}

		assert_response :forbidden
		assert_equal "Account already exist", flash[:error]
		assert_nil flash[:success]
	end

	test "should reject missing user name" do
		post register_path, params: {
			real_name: "Test User",
			email: "test@example.com",
			password: "12345678",
			confirmation: "12345678",
			home_location_id: locations(:one).id
		}

		assert_response :unprocessable_entity
		assert_equal "Could not create account", flash[:error]
		assert_nil flash[:success]
	end

	test "should reject missing real name" do
		post register_path, params: {
			user_name: "test",
			email: "test@example.com",
			password: "12345678",
			confirmation: "12345678",
			home_location_id: locations(:one).id
		}

		assert_response :unprocessable_entity
		assert_equal "Could not create account", flash[:error]
		assert_nil flash[:success]
	end

	test "should reject broken email" do
		post register_path, params: {
			user_name: "test",
			real_name: "Test User",
			password: "12345678",
			confirmation: "12345678",
			home_location_id: locations(:one).id
		}

		assert_response :unprocessable_entity
		assert_equal "Could not create account", flash[:error]
		assert_nil flash[:success]
	end

	test "should reject missing password" do
		post register_path, params: {
			user_name: "test",
			real_name: "Test User",
			email: "test@example.com",
			confirmation: "12345678",
			home_location_id: locations(:one).id
		}

		assert_response :unprocessable_entity
		assert_equal "Invalid password", flash[:error]
		assert_nil flash[:success]
	end

	test "should reject mismatching confirmation" do
		post register_path, params: {
			user_name: "test",
			real_name: "Test User",
			email: "test@example.com",
			password: "12345678",
			confirmation: "87654321",
			home_location_id: locations(:one).id
		}

		assert_response :unprocessable_entity
		assert_equal "Bad password confirmation", flash[:error]
		assert_nil flash[:success]
	end

	test "should get forgot-password" do
		get forgot_password_path

		assert_response :success
	end

	test "should post password-reset-request with valid email" do
		post password_reset_request_path, params: {
			email: "one@example.com"
		}
		follow_redirect!

		assert_response :success
		assert_equal password_reset_sent_path, path
	end

	test "post password-reset-request with invalid email should silently fail" do
		post password_reset_request_path, params: {
			email: "two@example.com"
		}
		follow_redirect!

		assert_response :success
		assert_equal password_reset_sent_path, path #We don't need another test
	end

	test "should get password-reset" do
		users(:one).save
		sgid = users(:one).signed_id(purpose: :password_reset)

		get password_reset_path(sgid)
		assert_response :success
	end

	test "should reset password if valid" do
		users(:one).save
		sgid = users(:one).signed_id(purpose: :password_reset)

		post password_reset_path(sgid), params: {
			password: "sillysampletext",
			confirmation: "sillysampletext"
		}
		follow_redirect!

		assert_response :success
		assert_equal login_path, path
		assert_equal "Your password has been updated", flash[:success]
		assert_nil flash[:error]
	end

	test "should reject resetting password if invalid" do
		users(:one).save
		sgid = users(:one).signed_id(purpose: :password_reset)

		post password_reset_path(sgid), params: {
			password: "",
			confirmation: ""
		}
		follow_redirect!

		assert_response :success
		assert_equal password_reset_path(sgid), path
		assert_equal "Invalid password", flash[:error]
		assert_nil flash[:success]
	end

	test "should reject resetting password if confirmation does not match" do
		users(:one).save
		sgid = users(:one).signed_id(purpose: :password_reset)

		post password_reset_path(sgid), params: {
			password: "sillysampletext",
			confirmation: "samplesillytext"
		}
		follow_redirect!

		assert_response :success
		assert_equal password_reset_path(sgid), path
		assert_equal "Password and confirmation do not match", flash[:error]
		assert_nil flash[:success]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
