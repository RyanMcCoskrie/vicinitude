require "test_helper"

class OrganisationControllerTest < ActionDispatch::IntegrationTest
	fixtures :locations, :users, :organisations, :memberships

	#TODO Figure out how to use Organisation fixtures without foreign
	#key violations
	def setup
		users(:one).save
		users(:two).save
		users(:three).save
		users(:four).save
		users(:five).save
		locations(:one).save
		organisations(:one).save
		@membership = memberships(:two)
		@membership.save
	end

	test "should redirect unless logged in" do
		get organisations_path
		assert_response :redirect

		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get organisations_path
		assert_response :success
	end

	test "Should get existing organisations" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get organisation_path( organisations(:one).name )

		assert_response :success
	end

	test "should display the new organisation form" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get new_organisation_path
		assert_response :success
	end

	test "should create valid organisations" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		post organisations_path, params: { organisation: {
			type: "Club",
			name: "Rugby Club",
			public: true,
			descr: "Get your boots on",
			location_id: Location.first.id
		}}

		assert_response :redirect
		follow_redirect!
		assert_response :success
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "should reject unauthroised updates to organisations" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		patch organisation_path(organisations(:one).name), params: { organisation: {
			type: "Club",
			name: "Local Rugby Football Club",
			public: false,
			descr: "Get your boots on!",
			location_id: organisations(:one).location_id
		}}

		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:error]
		assert_nil flash[:success]
	end

	test "should accept authorised updates to organisations" do
		post authenticate_path, params: {user_name: "five", password: "12345678"}

		patch organisation_path(organisations(:one).name), params: { organisation: {
			type: "Club",
			name: "Local Rugby Football Club",
			public: false,
			descr: "Get your boots on!",
			location_id: organisations(:one).location_id
		}}

		assert_response :redirect
		follow_redirect!
		assert_response :success
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "users can apply to organisations" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get organisation_apply_path(organisations(:one).name)

		assert_response :redirect
		follow_redirect!
		assert_response :success
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "users can't apply to organisations twice" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get organisation_apply_path(organisations(:one).name)
		get organisation_apply_path(organisations(:one).name)

		assert_response :redirect
		follow_redirect!
		assert_response :success
		assert_nil flash[:success]
		assert_not_nil flash[:error]
	end

	test "users can leave organisations" do
		post authenticate_path, params: {user_name: "admin", password: "12345678"}

		get organisation_leave_path(organisations(:one).name)

		assert_response :redirect
		follow_redirect!
		assert_response :success
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "non-members can subscribe to organisations" do
		post authenticate_path, params: {user_name: "three", password: "12345678"}

		get organisation_subscribe_path(organisations(:one).name)

		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "non-members can unsubscribe from organisations" do
		post authenticate_path, params: {user_name: "one", password: "12345678"}

		get organisation_unsubscribe_path(organisations(:one).name)

		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:success]
		assert_nil flash[:error]
	end

	test "moderators can send invitations" do
		post authenticate_path, params: {user_name: "five", password: "12345678"}

		get organisation_path(organisations(:one).name)
		assert_response :success
		assert_nil flash[:error]

		get organisation_invite_members_path(organisations(:one).name)
		assert_response :success
		assert_nil flash[:error]

		assert_emails 4 do
			post organisation_send_invitations_path(organisations(:one).name),
				params: {users: [
					"", #A side effect of the view we have to test against
					users(:one).id,
					users(:two).id,
					users(:three).id,
					users(:four).id
				]}
			assert_response :redirect
			follow_redirect!
			assert_response :success
			assert_nil flash[:error]
			assert_not_nil flash[:success]
		end
	end

	test "registrant can initiate transfer of registration" do
		post authenticate_path, params: {user_name: "five", password: "12345678"}

		get organisation_transfer_path(organisations(:one).name)
		assert_response :success

		post organisation_begin_transfer_path(organisations(:one).name),
			params: {user_id: users(:one).id}
		assert_response :redirect
		follow_redirect!
		assert_nil flash[:error]
		assert_not_nil flash[:success]

		assert_emails 1
	end

	test "logged out user can accept registration transfer" do
		org_sgid = Organisation.first.signed_id
		user_sgid = User.first.signed_id

		get organisation_accept_transfer_path(org_sgid, user_sgid)
		assert_response :success
		assert_nil flash[:error]
		assert_not_nil flash[:success]
		assert_emails 1
		assert_equal Organisation.first.registrant.id, User.first.id
	end

	test "logged out user can reject registration transfer" do
		org_sgid = organisations(:one).signed_id
		user_sgid = users(:one).signed_id

		get organisation_reject_transfer_path(org_sgid, user_sgid)
		assert_response :success
		assert_nil flash[:error]
		assert_not_nil flash[:success]
		assert_emails 1
		assert_equal Organisation.last.registrant, organisations(:one).registrant
	end

	test "Organisation registrant can appoint moderators" do
		post authenticate_path, params: {user_name: "five", password: "12345678"}

		get organisation_moderators_path(organisations(:one).name)
		assert_response :success

		post organisation_moderators_path(organisations(:one).name),
			params: {users: {users(:two).id => "1"}}
		assert_response :redirect
		follow_redirect!
		assert_nil flash[:error]
		assert_not_nil flash[:success]

		assert_equal 6, Membership.find_by(
			organisation: organisations(:one),
			member: users(:two)
		).priv
	end

	test "Other users can not appoint moderators" do
		post authenticate_path, params: {user_name: "two", password: "12345678"}

		get organisation_moderators_path(organisations(:one).name)
		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:error]
		assert_nil flash[:success]

		post organisation_moderators_path(organisations(:one).name),
			params: {users: {users(:two).id => "1"}}
		assert_response :redirect
		follow_redirect!
		assert_not_nil flash[:error]
		assert_nil flash[:success]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
