require "test_helper"
require "action_mailer"

class OrganisationMailerTest < ActionMailer::TestCase
	fixtures :users, :locations, :organisations

	def setup
		locations(:one).save
		users(:one).save
		users(:two).save
		users(:five).save
		organisations(:one).save

		@membership = Membership.create(
			member: User.second,
			organisation: organisations(:one),
			priv: 1
		)
		@sgid = @membership.signed_id(expires_in: 7.days)
	end

	test 'send new application notice' do
		assert_emails 0
		email = OrganisationMailer.new_applicant(
			organisations(:one),
			User.second,
			@sgid
		)
		assert_emails 1 do
			email.deliver_later
		end

		assert_equal [organisations(:one).registrant.email], email.to
		assert_equal [Rails.configuration.vicinitude[:admin_email]], email.from
	end

	test 'send invitation notice' do
		email = OrganisationMailer.invite_user(organisations(:one), users(:two))

		assert_emails 1 do
			email.deliver_later!
		end

		assert_equal [users(:two).email], email.to
		assert_equal [Rails.configuration.vicinitude[:admin_email]], email.from
	end

	test 'send invitation accepted notice' do
		assert_emails 0

		email = OrganisationMailer.invitation_accepted(organisations(:one), users(:one))

		assert_emails 1 do
			email.deliver_later!
		end

		assert_equal [organisations(:one).registrant.email], email.to
		assert_equal [Rails.configuration.vicinitude[:admin_email]], email.from
	end

	test 'send invitation rejected notice' do
		email = OrganisationMailer.invitation_rejected(organisations(:one), users(:one))

		assert_emails 1 do
			email.deliver_now!
		end

		assert_equal [organisations(:one).registrant.email], email.to
		assert_equal [Rails.configuration.vicinitude[:admin_email]], email.from
	end

	test 'send registration transfer offer' do
		email = OrganisationMailer.transfer_offered(
			organisations(:one),
			users(:one),
			organisations(:one).signed_id(expires_in: 7.days),
			users(:one).signed_id(expires_in: 7.days)
		)

		assert_emails 1 do
			email.deliver_now!
		end

		assert_equal [users(:one).email], email.to
		assert_equal [Rails.configuration.vicinitude[:admin_email]], email.from
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
