require "test_helper"
require "action_mailer"

class FeedbackMailerTest < ActionMailer::TestCase
	fixtures :locations
	def setup
		locations(:one).save
		@user = User.new
		@user.user_name = "test"
		@user.real_name = "Test User"
		@user.email = "address@provider.tld"
		@user.password = "examplepassword"
		@user.home_location = locations(:one)
		@user.priv = 0
		@user.verification_code = SecureRandom.hex(4)
		@user.save!
	end

	test 'send general feedback' do
		assert_emails 0

		email = FeedbackMailer.general_feedback(
			@user.user_name,
			@user.real_name,
			"Email subject",
			"Test message.\n\nPlease Ignore."
		)
		assert_emails 1 do
			email.deliver_later
		end

		assert_equal email.to, [Rails.configuration.action_mailer.smtp_settings[:user_name]]
		assert_equal email.from, [Rails.configuration.action_mailer.smtp_settings[:user_name]]
		assert_equal email.subject, "Feedback from test (Test User): Email subject"
	end

	test 'send location requests' do
		assert_emails 0

		email = FeedbackMailer.request_location(
			@user.user_name,
			@user.real_name,
			"Nowhereville town square",
			"Lattitude -42.5, Longitude -172.5"
		)
		assert_emails 1 do
			email.deliver_later
		end

		assert_equal email.to, [Rails.configuration.action_mailer.smtp_settings[:user_name]]
		assert_equal email.from, [Rails.configuration.action_mailer.smtp_settings[:user_name]]
		assert_equal email.subject, "Missing location: Nowhereville town square"
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
