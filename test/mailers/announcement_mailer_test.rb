require "test_helper"
require "action_mailer"

class AnnouncementMailerTest < ActionMailer::TestCase
	test 'generate general announcement' do
		email = AnnouncementMailer.general_announcement(
			"test@example.com",
			"Down for maintenance",
			"We will be down for five minutes at midnight"
		)

		assert_equal email.from, [Rails.configuration.action_mailer.smtp_settings[:user_name]]
		assert_equal email.subject, "Down for maintenance"
	end
	test 'send general announcement to all users' do
		assert_emails 0

		assert_emails User.count do
			User.all.each do |user|
				email = AnnouncementMailer.general_announcement(
					user.email,
					"Down for maintenance",
					"We will be down for five minutes at midnight"
				)
				email.deliver_later
			end
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
