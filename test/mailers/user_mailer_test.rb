require "test_helper"
require "action_mailer"

class UserMailerTest < ActionMailer::TestCase
	fixtures :locations

	def setup
		locations(:one).save

		@user = User.new
		@user.user_name = "test"
		@user.real_name = "Test User"
		@user.email = "address@provider.tld"
		@user.password = "examplepassword"
		@user.home_location = locations(:one)
		@user.priv = 0
		@user.verification_code = SecureRandom.hex(4)
		@user.save!
	end

	test 'send verification request' do
		assert_emails 0
		email = UserMailer.verification_request(
			@user.user_name,
			@user.real_name,
			@user.verification_code,
			"123 Fake rd\nNowhereville"
		)
		assert_emails 1 do
			email.deliver_later
		end

		assert_equal [Rails.configuration.action_mailer.smtp_settings[:user_name]], email.to
		assert_equal [Rails.configuration.action_mailer.smtp_settings[:user_name]], email.from
		assert_equal "Verification request from #{@user.user_name}", email.subject
		assert_match @user.verification_code, email.body.encoded
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
