Rails.application.routes.draw do
	concern :commentable do
		post '/comments' => :create_comment, as: :comments
		delete '/commments/:id' => :destroy_comment, as: :comment
	end

	concern :subscribable do
		get :subscribe
		get :unsubscribe
	end

	root "meta#index"
	scope controller: :meta do
		get '/about' => :about
		get '/contact' => :contact
		get '/terms' => :terms
	end

	scope controller: :session do
		get '/signup' => :signup
		post '/register' => :register
		get '/login' => :login
		post '/authenticate' => :authenticate
		get '/logout' => :logout
		get '/forgot-password' => :forgot_password
		post '/password-reset-request' => :password_reset_request
		get '/password-reset/:sgid' => :password_reset, as: :password_reset
		post '/password-reset/:sgid' => :submit_password_reset
		get '/password-reset-sent' => :password_reset_sent
	end

	scope controller: :home do
		get '/home' => :index
		get '/profile' => :profile
		post '/profile/request_verification' => :request_verification
		post '/profile/confirm_verification' => :confirm_verification
		get '/profile/edit' => :edit_profile
		patch '/profile' => :update_profile
		delete '/profile' => :delete_profile
	end

	resources :locations, param: :name, only: [:index, :show] do
		concerns :subscribable
	end

	resources :events do
		concerns :commentable
	end

	resources :organisations, param: :name do
		concerns :subscribable
		get 'moderators' => :choose_moderators
		post 'moderators' => :update_moderators
		get :apply
		get :leave
		get :invite_members
		post :send_invitations
		get :transfer
		post :begin_transfer
		resources :discussions do
			concerns :commentable
		end
	end

	#Passing registrant status to another user
	get '/organisations/:org_sgid/:user_sgid/accept-transfer' => "organisations#accept_transfer", as: :organisation_accept_transfer
	get '/organisations/:org_sgid/:user_sgid/reject-transfer' => "organisations#reject_transfer", as: :organisation_reject_transfer

	#User applied for membership
	get '/memberships/:sgid/accept_application' => "memberships#accept_application", as: :membership_accept_application
	get '/memberships/:sgid/reject_application' => "memberships#reject_application", as: :membership_reject_application

	#User offerred membership
	get '/memberships/:org_sgid/:user_sgid/accept-invitation' => "memberships#accept_invitation", as: :membership_accept_invitation
	get '/memberships/:org_sgid/:user_sgid/reject-invitation' => "memberships#reject_invitation", as: :membership_reject_invitation

	scope controller: :feedback do
		get '/feedback' => :index
		get '/feedback/general' => :general
		get '/feedback/missing_location' => :location
		post '/feedback/general' => :submit_general
		post '/feedback/missing_location' => :submit_location
	end

	namespace :console, controller: :main do
		root "main#index"

		resources :users
		resources :locations do
			post 'from_csv', on: :collection
			get 'to_csv', on: :collection
		end
		resources :events
		resources :organisations

		get 'config' => :config_index
		get 'config/branding' => :branding
		post 'config/branding' => :update_branding
		post 'config/logo' => :update_logo
		get 'config/map' => :map
		post 'config/map' => :update_map
		get 'config/terms' => :terms
		post 'config/terms' => :update_terms
		get 'config/export' => :export_config
		post 'config/import' => :import_config

		get 'logs/:hours/hours' => :logs, as: 'logs'

		get 'announcement' => :announcement
		post 'announcement' => :submit_announcement
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
