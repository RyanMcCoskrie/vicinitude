class MetaController < ApplicationController
	layout "meta"
	before_action :switch_to_home, only: [:index]


	def index
		@subtitle = "Welcome!"
	end

	def about
		@subtitle = "About"
	end

	def contact
		@subtitle = "Contact"
	end

	def terms
		@subtitle = "Terms of Service"
	end


	private

	def switch_to_home
		redirect_to home_path if current_user
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
