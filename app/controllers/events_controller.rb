class EventsController < ApplicationController
	include EventsConcern
	include CommentsConcern
	before_action :logged_in_check
	after_action :send_upcoming_event_mailer, only: :create
	after_action :send_updated_event_mailer, only: :update


	def index
		@subtitle = "Events"
		now = Time.now
		@current = Event.where("start_time < ? and end_time > ?", now, now)
		@upcoming = Event.where("start_time > ?", now)
		@pagy, @previous = pagy( Event.where("end_time < ?", now) )
	end

	def new
		@event = Event.new
		@locations = Location.all.order(:name)
		@subtitle = "Create Event"

		if params.has_key? :location
			begin
				@event.location = Location.find_by_name(params[:location])
			rescue ActiveRecord::RecordNotFound => e
				redirect_to locations_path, flash:
					{error: "Location not found"}
			end
		end

		@event.start_time = default_start_time()
		@event.end_time = default_end_time()
	end

	def create
		begin
			@event = Event.new

			@event.name = params[:event][:name]
			@event.descr = params[:event][:descr]
			@event.user_id = current_user.id
			@event.start_time = parse_time(params[:event][:start_time])
			@event.end_time = parse_time(params[:event][:end_time])
			@event.location_id = params[:event][:location_id]
			if params[:event][:organisation_id].empty? == false
				@event.organisation = Organisation.find(
					params[:event][:organisation_id]
				)
				check_if_moderator(@event.organisation)
			else
				@event.organisation = nil
			end

			@event.save!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'event', action: 'create',
				entry_id: @event.id, entry_name: @event.name
			}).save

			redirect_to @event, flash: {success: "Event Created"}
		rescue Date::Error => e
			@subtitle = "Create Event"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Invalid time or date"
			render :new, status: :unprocessable_entity
		rescue IOError => e
			@subtitle = "Create Event"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Insufficient permissions"
			render :new, status: :unprocessable_entity
		rescue ActiveRecord::RecordNotFound => e
			@subtitle = "Create Event"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Could not find location or organisation"
			render :new, status: :unprocessable_entity
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Create Event"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Invalid parameters"
			render :new, status: :unprocessable_entity
		end
	end

	def show
		begin
			@event = Event.find params[:id]
			@subtitle = @event.name
			@comment = Comment.new
		rescue ActiveRecord::RecordNotFound => e
			redirect_to events_path, flash: {error: "Could not find event"}
		end
	end

	def edit
		begin
			@event = Event.find params[:id]
			raise IOError.new if @event.can_edit?(current_user) == false
			@subtitle = "Editing #{@event.name}"
			@locations = Location.all.order(:name)
		rescue ActiveRecord::RecordNotFound => e
			redirect_to events_path,
				flash: {error: "Could not find event"}
		end
	end

	def update
		begin
			@event = Event.find params[:id]
			@subtitle = "Editing #{@event.name}"
			@comment = Comment.new

			raise IOError.new if @event.can_edit?(current_user) == false
			@event.name = params[:event][:name]
			@event.descr = params[:event][:descr]
			@event.start_time = parse_time(params[:event][:start_time])
			@event.end_time = parse_time(params[:event][:end_time])
			if params[:event][:organisation_id].empty? == false
				@event.organisation = Organisation.find(
					params[:event][:organisation_id]
				)
				check_if_moderator(@event.organisation)
			else
				@event.organisation = nil
			end
			@event.location_id = params[:event][:location_id]


			@event.save!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'event', action: 'update',
				entry_name: @event.name, entry_id: @event.id
			}).save

			redirect_to @event, flash: {success: "Event updated"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to events_path,
				flash: {error: "Could not find event, location or organisation"}
		rescue Date::Error => e
			@subtitle = "Editing #{@event.name}"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Invalid time or date"
			render :edit, status: :unprocessable_entity
		rescue IOError => e
			@subtitle = @event.name
			@locations = Location.all.order(:name)
			flash.now[:error] = "Insufficient permissions"
			render :show, status: :forbidden
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Editing #{@event.name}"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Invalid parameters"
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		begin
			@event = Event.find params[:id]
			raise IOError.new if @event.can_edit?(current_user) == false

			@event.destroy!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'event', action: 'delete',
				entry_name: @event.name, entry_id: @event.id
			}).save

			redirect_to events_path, flash: {success: "Event deleted"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to events_path, flash: {error: "Could not find event"}
		rescue IOError => e
			@subtitle = @event.name
			flash.now[:error] = "Insufficient permissions"
			render :show, status: :internal_server_error
		rescue ActiveRecord::RecordNotDestroyed
			@subtitle = @event.name
			flash.now[:error] = "Could not delete event"
			render :show, status: :forbidden
		end
	end

	def create_comment
		begin
			event = Event.find params[:event_id]


			perform_create_comment event, params[:comment][:body]

			redirect_to event_path(event),
				flash: {success: "Comment posted"}
		rescue IOError => e
			redirect_to event_path(event),
				flash: {error: "Insufficiant permission"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to event_path(event),
				flash: {error: "Invalid parameters"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to events_path,
				flash: {error: "Could not find event"}
		end
	end

	def destroy_comment
		begin
			event = Event.find params[:event_id]

			perform_delete_comment params[:id]

			redirect_to event_path(event),
				flash: {success: "Comment deleted"}
		rescue IOError => e
			redirect_to event_path(event),
				flash: {error: "Insufficiant permission"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to events_path,
				flash: {error: "Could not find event or comment"}
		rescue ActiveRecord::RecordNotDestroyed => e
			redirect_to event_path(event),
				flash: {error: "Could not delevent comment"}
		end
	end

private
	def send_upcoming_event_mailer
		EventMailer.upcoming_event(@event).deliver_now
	end

	def send_updated_event_mailer
		EventMailer.updated_event(@event).deliver_now
	end

	def check_if_moderator(org)
		membership = Membership.find_by(
			organisation: org,
			member: current_user
		)
		if membership == nil or membership.moderator? == false
			raise IOError.new
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
