class DiscussionsController < ApplicationController
	include CommentsConcern
	before_action :logged_in_check

	def index
		begin
			@org = Organisation.find_by_name params[:organisation_name]
			@pagy, @discssns = pagy( @org.discussions.order time: :desc )
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path,
				flash: {error: "Could not find organisation"}
		end
	end

	def new
		begin
			@org = Organisation.find_by_name params[:organisation_name]

			@discssn = Discussion.new
			@comment = Comment.new
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path,
				flash: {error: "Could not find organisation"}
		end
	end

	def create
		begin
			discssn = Discussion.new
			discssn.poster_id = current_user.id
			org = Organisation.find_by_name params[:organisation_name]
			discssn.name = params[:discussion][:name]
			discssn.organisation = org
			discssn.time = Time.now

			discssn.save!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'discussions', action: 'create',
				parent_name: org.name, parent_id: org.id
			}).save

			perform_create_comment discssn, params[:comment][:body]

			redirect_to organisation_discussion_path(org.name, discssn.id),
				flash: {success: "Discussion posted"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path,
				flash: {error: "Could not find organisation"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to organisation_path(params[:organisation_name]),
				flash: {error: "Invalid parameters"}
		end
	end

	def show
		begin
			@org = Organisation.find_by name: params[:organisation_name]
			@discssn = Discussion.find params[:id]
			@comments = @discssn.comments.order(time: :asc)
			@comment = Comment.new
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisation_path(params[:organisation_name]), flash:
				{error: "Could not find discussion"}
		end
	end

	def destroy
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]
			@discssn = Discussion.find params[:id]
			raise ArgumentError.new if @discssn.organisation != @org
			raise IOError.new if @discssn.can_edit?(current_user) == false
			@discssn.destroy!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'discussions', action: 'delete',
				parent_name: @discssn.organisation.name,
				parent_id: @discssn.organisation_id,
				entry_id: @discssn.id
			}).save

			redirect_to organisation_discussions_path(@org.name), flash:
				{success: "Discussion deleted"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisation_discussions_path(params[:organisation_name]), flash:
				{error: "Could not find discussion or organisation"}
		rescue ArgumentError => e
			redirect_to organisations_path, flash:
				{error: "Organisation and discussion does not match"}
		rescue ActiveRecord::RecordNotDestroyed => e
			redirect_to discussion_path(@org, @discssn), flash:
				{error: "Could not delete discussion"}
		rescue IOError =>e
			redirect_to discussion_path(@org, @discssn), flash:
				{error: "Insufficient permissions"}
		end
	end

	def create_comment
		begin
			discssn = Discussion.find params[:discussion_id]

			org = discssn.organisation
			raise IOError.new if org.member?(current_user) == false

			perform_create_comment discssn, params[:comment][:body]

			redirect_to organisation_discussion_path(
				params[:organisation_name],
				params[:discussion_id]
			), flash: {success: "Comment posted"}
		rescue IOError => e
			redirect_to organisation_discussion_path(
				params[:organisation_name],
				params[:discussion_id]
			), flash: {error: "Insufficient permission"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisation_discussions_path(
				params[:organisation_name]
			), flash: {error: "Discussion not found"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to organisation_discussion_path(
				params[:organisation_name],
				params[:discussion_id]
			), flash: {error: "Invalid parameters"}
		end
	end

	def destroy_comment
		begin
			perform_delete_comment params[:id]

			redirect_to organisation_discussion_path(
				params[:organisation_name],
				params[:discussion_id]
			), flash: {success: "Comment deleted"}
		rescue IOError => e
			redirect_to discussion_path(
				params[:organisation_name],
				params[:discussion_id]
			), flash: {error: "Insufficiant permission"}
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
