require 'yaml'

module Console

class MainController < ApplicationController
	layout "console"
	before_action :is_admin_check

	def index
		@subtitle = "Home"

		@userCount = User.count
		@locationCount = Location.count
		@eventCount = Event.count
	end

	def config_index
		@subtitle = "Configuration"
	end

	def branding
		@subtitle = "Branding Configuration"
	end

	def update_branding
		Rails.configuration.vicinitude[:site_name] = params[:site_name]
		Rails.configuration.vicinitude[:town_name] = params[:town_name]
		Rails.configuration.vicinitude[:admin_email] = params[:admin_email]
		Rails.configuration.vicinitude[:domain_name] = params[:domain_name]
		Rails.configuration.vicinitude[:notice] = params[:notice]
		Rails.configuration.vicinitude[:footer_message] = params[:footer_message]

		save_config!

		redirect_to console_config_branding_path,
			flash: {success: "Configuration updated"}
	end

	def update_logo
		if params[:logo_32px]
			file = File.open('app/assets/images/logo_32px.png', 'wb')
			file << params[:logo_32px].read
			file.close
		end

		if params[:logo_64px]
			file = File.open('app/assets/images/logo_64px.png', 'wb')
			file << params[:logo_64px].read
			file.close
		end

		if params[:logo_200px]
			file = File.open('app/assets/images/logo_200px.png', 'wb')
			file << params[:logo_200px].read
			file.close
		end

		redirect_to console_config_branding_path,
			flash: {success: "Logo updated"}
	end

	def map
		@subtitle = "Map Configuration"
		@map_sources = MapSource.all.order(:id)
		@current_source = Rails.configuration.vicinitude[:map_source]
	end

	def update_map
		Rails.configuration.vicinitude[:map_source] = params[:source_id]
		if params[:source_url] and params[:source_is_vector]
			custom_source = MapSource.find(1)
			custom_source.url = params[:source_url]
			custom_source.is_vector = params[:source_is_vector]
			custom_source.attribution = params[:source_attribution]
			custom_source.save
		end
		Rails.configuration.vicinitude[:map_centre][:lat] = params[:lat]
		Rails.configuration.vicinitude[:map_centre][:lng] = params[:lng]
		Rails.configuration.vicinitude[:map_centre][:high_zoom] = params[:high_zoom]
		Rails.configuration.vicinitude[:map_centre][:low_zoom] = params[:low_zoom]

		save_config!

		redirect_to console_config_map_path,
			flash: {success: "Configuration updated"}
	end

	def terms
		@subtitle = "Terms of Service Configuration"
	end

	def update_terms
		Rails.configuration.vicinitude[:terms] = params[:terms]

		save_config!

		redirect_to console_config_terms_path,
			flash: {success: "Terms of Service updated"}
	end

	def export_config
		send_file 'config/vicinitude.yml'
	end

	def import_config
		yaml = YAML.load(params[:file].read)[:shared]

		Rails.configuration.vicinitude[:site_name] = yaml[:site_name]
		Rails.configuration.vicinitude[:town_name] = yaml[:town_name]
		Rails.configuration.vicinitude[:admin_email] = yaml[:admin_email]
		Rails.configuration.vicinitude[:domain_name] = yaml[:domain_name]
		Rails.configuration.vicinitude[:notice] = yaml[:notice]
		Rails.configuration.vicinitude[:footer_message] = yaml[:footer_message]

		Rails.configuration.vicinitude[:map_source] = yaml[:map_source]
		Rails.configuration.vicinitude[:map_centre][:lat] = yaml[:map_centre][:lat]
		Rails.configuration.vicinitude[:map_centre][:lng] = yaml[:map_centre][:lng]
		Rails.configuration.vicinitude[:map_centre][:high_zoom] = yaml[:map_centre][:high_zoom]
		Rails.configuration.vicinitude[:map_centre][:low_zoom] = yaml[:map_centre][:low_zoom]

		Rails.configuration.vicinitude[:terms] = yaml[:terms]

		save_config!

		redirect_to console_config_path,
			flash: {success: "Configuration updated"}
	end

	def logs
		@hours = params[:hours].to_i
		now = Time.now
		@action_logs = ActionLog.where("created_at > '#{now - @hours.hours}'")
		@subtitle = "User actions in the last #{@hours} hours"
	end

	def announcement
		@subtitle = "Announcement to users"
	end

	def submit_announcement
		User.all.each do |user|
			AnnouncementMailer.general_announcement(
				user,
				params[:subject],
				params[:body]
			).deliver_later
		end

		redirect_to console_root_path,
			flash: {success: "Your announcement has been made"}
	end

	#TODO Figure out how best this could trigger an error condition
	private def save_config!
		yaml = {
			shared: {
				site_name: Rails.configuration.vicinitude[:site_name],
				town_name: Rails.configuration.vicinitude[:town_name],
				admin_email: Rails.configuration.vicinitude[:admin_email],
				domain_name: Rails.configuration.vicinitude[:domain_name],
				notice: Rails.configuration.vicinitude[:notice],
				footer_message: Rails.configuration.vicinitude[:footer_message],
				map_source: Rails.configuration.vicinitude[:map_source],
				map_centre: {
					lat: Rails.configuration.vicinitude[:map_centre][:lat],
					lng: Rails.configuration.vicinitude[:map_centre][:lng],
					high_zoom: Rails.configuration.vicinitude[:map_centre][:high_zoom],
					low_zoom: Rails.configuration.vicinitude[:map_centre][:low_zoom]
				},
				terms: Rails.configuration.vicinitude[:terms]
			}
		}.to_yaml

		file = File.open("config/vicinitude.yml", "w")
		file.write yaml
		file.close
	end
end

end

# vim: ts=4 sts=4 sw=0 noexpandtab
