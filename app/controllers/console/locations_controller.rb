require 'csv'


module Console

class LocationsController < ApplicationController
	layout "console"
	before_action :is_admin_check

	def index
		@subtitle = "Locations"

		@locations = Location.where(ancestry: nil).order(:name)
	end

	def new
		@subtitle = "Create Location"

		@locations = Location.all.order(:name)
		@location = Location.new
	end

	def create
		begin
			@location = Location.new
			@location.name = params[:name]
			@location.descr = params[:descr]
			@location.parent_id = params[:parent_id]
			@location.address = params[:address]
			@location.lat = params[:lat]
			@location.lng = params[:lng]

			@location.save!

			redirect_to console_location_path(@location),
				flash: {success: "Location created"}
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Create Location"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Invalid parameters"
			render :new, status: :unprocessable_entity
		end
	end

	def show
		begin
			@location = Location.find params[:id]
			@subtitle = @location.name
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_locations_path,
				flash: {error: "Could not find location"}
		end
	end

	def edit
		begin
			@location = Location.find params[:id]
			@locations = Location.all.order(:name)
			@subtitle = "Editing #{@location.name}"
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_locations_path,
				flash: {error: "Could not find location"}
		end
	end

	def update
		begin
			@location = Location.find params[:id]

			@location.name = params[:name]
			@location.descr = params[:descr]
			@location.parent_id = params[:parent_id]
			@location.address = params[:address]
			@location.lat = params[:lat]
			@location.lng = params[:lng]

			@location.save!

			redirect_to console_location_path(@location),
				flash: {success: "Location updated"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_locations_path,
				flash: {error: "Could not find location"}
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Editing #{@location.name}"
			@locations = Location.all.order(:name)
			flash.now[:error] = "Invalid parameters"
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		begin
			@location = Location.find params[:id]

			@location.destroy!

			redirect_to console_locations_path,
				flash: {success: "Location deleted"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_locations_path,
				flash: {error: "Could not find location"}
		rescue ActiveRecord::RecordNotDestroyed => e
			@subtitle = @location.name
			flash.now[:error] = "Could not delete location"
			render :show, status: :internal_server_error
		end
	end

	def to_csv
		attributes = %w{id name ancestry descr address lat lng}

		data = CSV.generate do |csv|
			Location.order(:id).each do |loc|
				csv << attributes.map{|attr| loc.send attr}
			end
		end

		send_data data, filename: 'locations.csv'
	end

	def from_csv
		locs = CSV.parse(params[:file].tempfile)

		User.all.update(home_location_id: nil)

		Location.destroy_all

		locs.each{|l| Location.create(id: l[0],
									  name: l[1],
									  ancestry: l[2],
									  descr: l[3],
									  address: l[4],
									  lat: l[5],
									  lng: l[6])}

		#Reset primary key
		connection = ActiveRecord::Base.connection
		connection.execute("SELECT setval(pg_get_serial_sequence('locations', 'id'), MAX(id)) FROM locations;")

		redirect_to console_locations_path
	end
end

end

# vim: ts=4 sts=4 sw=0 noexpandtab
