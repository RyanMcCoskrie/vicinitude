module Console

class UsersController < ApplicationController
	layout "console"
	before_action :is_admin_check

	def index
		@subtitle = "Users"
		@pagy, @users = pagy User.all.order(:user_name)
	end

	def new
		@subtitle = "Create User"
		@locations = Location.where(ancestry: nil)
		@user = User.new
	end

	def create
		begin
			raise BadConfirmation if params[:confirmation] != params[:password]

			@user = User.new
			@user.user_name = params[:user_name]
			@user.real_name = params[:real_name]
			@user.email = params[:email]
			@user.password = params[:password]
			if params[:home_location_id].blank? == false
				@user.home_location = Location.find params[:home_location_id]
			else
				@user.home_location = nil
			end
			#Do not allow blank permissions
			@user.priv ? @user.priv = params[:priv] : @user.priv = 1
			@user.save!
			redirect_to console_user_path(@user),
				flash: {success: "User created"}
		rescue BadConfirmation => e
			@subtitle = "Create User"
			flash.now[:error] = "Bad password confirmation"
			@locations = Location.where(ancestry: nil)
			render :new, status: :unprocessable_entity
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Create User"
			flash.now[:error] = "Invalid parameters"
			@locations = Location.where(ancestry: nil)
			render :new, status: :unprocessable_entity
		end
	end

	def show
		begin
			@user = User.find params[:id]
			@subtitle = @user.user_name
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_users_path,
				flash: {error: "Could not find User"}
		end
	end

	def edit
		begin
			@user = User.find params[:id]
			@subtitle = "Editing #{@user.user_name}"
			@locations = Location.where(ancestry: nil)
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_users_path,
				flash: {error: "Could not find User"}
		end
	end

	def update
		begin
			raise BadConfirmation if params[:confirmation] != params[:password]

			@user = User.find params[:id]

			@user.user_name = params[:user_name]
			@user.real_name = params[:real_name]
			@user.email = params[:email]
			@user.password = params[:password] unless params[:password].empty?
			if params[:home_location_id].blank? == false
				@user.home_location = Location.find params[:home_location_id]
			else
				@user.home_location = nil
			end
			#Do not allow blank permissions
			@user.priv = params[:priv]
			@user.save!
			redirect_to console_user_path(@user),
				flash: {success: "Updated User"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_users_path,
				flash: {error: "Could not find User or Location"}
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Editing #{@user.user_name}"
			flash.now[:error] = "Invalid parameters"
			@locations = Location.where(ancestry: nil)
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		begin
			@user = User.find params[:id]
			if @user.id == 1
				@subtitle = @user.user_name
				flash.now[:error] = "Root user can not be deleted"
				render :show
			else
				@user.destroy!
				redirect_to console_users_path,
					flash: {success: "Deleted user"}
			end
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_users_path,
				flash: {error: "Could not find User"}
		rescue ActiveRecord::RecordNotDestroyed => e
			@subtitle = @user.user_name
			flash.now[:error] = "Could not delete user"
			render :show, status: :internal_server_errror
		end
	end
end

end

# vim: ts=4 sts=4 sw=0 noexpandtab
