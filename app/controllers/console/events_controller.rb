module Console

class EventsController < ApplicationController
	include EventsConcern
	layout "console"
	before_action :is_admin_check

	def index
		@subtitle = "Events"
		@pagy, @events = pagy(Event.all)
	end

	def new
		@subtitle = "Create event"
		@event = Event.new

		@event.start_time = default_start_time()
		@event.end_time = default_end_time()
		@locations = Location.all.order(:name)
		@users = User.all.order(:user_name)
		@organisations = Organisation.all.order(:name)
	end

	def create
		begin
			@event = Event.new

			@event.name = params[:name]
			@event.descr = params[:descr]
			@event.start_time = parse_time(params[:event][:start_time])
			@event.end_time = parse_time(params[:event][:end_time])
			@event.user_id = params[:user_id]
			@event.location_id = params[:location_id]
			if params[:event][:organisation_id].empty? == false
				@event.organisation = Organisation.find(
					params[:event][:organisation_id]
				)
			else
				@event.organisation = nil
			end

			@event.save!
			redirect_to console_event_path(@event),
				flash: {success: "Event created"}

		rescue Date::Error => e
			@subtitle = "Create event"
			flash.now[:error] = "Invalid time or date"
			@event.start_time = default_start_time()
			@event.end_time = default_end_time()
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			@organisations = Organisation.all.order(:name)
			render :new, status: :unprocessable_entity
		rescue IOError => e
			@subtitle = "Create Event"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			@organisations = Organisation.all.order(:name)
			flash.now[:error] = "User is not a member of organisation"
			render :new, status: :unprocessable_entity
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Create event"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			@organisations = Organisation.all.order(:name)
			flash.now[:error] = "Invalid parameters"
			render :new, status: :unprocessable_entity
		end
	end

	def show
		begin
			@event = Event.find params[:id]
			@subtitle = @event.name
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_events_path,
				flash: {error: "Could not find event"}
		end
	end

	def edit
		begin
			@event = Event.find params[:id]
			@subtitle = "Editing #{@event.name}"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			@organisations = Organisation.all.order(:name)
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_events_path,
				flash: {error: "Could not find event"}
		end
	end

	def update
		begin
			@event = Event.find params[:id]

			@event.name = params[:event][:name]
			@event.descr = params[:event][:descr]
			@event.start_time = parse_time(params[:event][:start_time])
			@event.end_time = parse_time(params[:event][:end_time])
			@event.user_id = params[:event][:user_id]
			@event.location_id = params[:event][:location_id]
			if params[:event][:organisation_id].empty? == false
				@event.organisation = Organisation.find(
					params[:event][:organisation_id]
				)
			else
				@event.organisation = nil
			end

			@event.save!
			redirect_to console_event_path(@event),
				flash: {success: "Event updated"}
		rescue ActiveRecord::RecordNotFound => e
			@subtitle = "Editing #{@event.name}"
			redirect_to console_events_path,
				flash: {error: "Could not find event"}
		rescue Date::Error => e
			@subtitle = "Editing #{@event.name}"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			@organisations = Organisation.all.order(:name)
			flash.now[:error] = "Invalid time or date"
			render :edit, status: :unprocessable_entity
		rescue IOError => e
			@subtitle = "Create Event"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			@organisations = Organisation.all.order(:name)
			flash.now[:error] = "User is not a member of organisation"
			render :new, status: :unprocessable_entity
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Editing #{@event.name}"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			@organisations = Organisation.all.order(:name)
			flash.now[:error] = "Invalid parameters"
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		begin
			event = Event.find params[:id]

			event.destroy!
			redirect_to console_events_path, flash: {success: "Event deleted"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_events_path,
				flash: {error: "Could not find event"}
		rescue ActiveRecord::RecordNotDestroyed => e
			@subtitle = @event.name
			flash.now[:error] = "Could not delete event"
			render :show, status: :internal_server_error
		end
	end
end

end

# vim: ts=4 sts=4 sw=0 noexpandtab
