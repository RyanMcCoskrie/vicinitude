module Console

class OrganisationsController < ApplicationController
	layout "console"
	before_action :is_admin_check


	def index
		@subtitle = "Organisations"

		@pagy, @orgs = pagy( Organisation.order(:name) )
	end

	def new
		@org = Organisation.new
		@locations = Location.all.order(:name)
		@users = User.all.order(:user_name)
		@subtitle = "Create Organisation"
	end

	def create
		begin
			@org = Organisation.new

			@org.name = params[:organisation][:name]
			@org.type = params[:organisation][:type]
			@org.public = params[:organisation][:public]
			@org.descr = params[:organisation][:descr]
			@org.registrant_id = params[:organisation][:registrant_id]
			@org.location_id = params[:organisation][:location_id]

			@org.save!

			redirect_to console_organisation_path(@org.becomes(Organisation)),
				flash: {success: "Organisation Created"}
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Create Organisation"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			flash.now[:error] = "Invalid parameters"
			render :new, status: :unprocessable_entity
		end
	end

	def show
		begin
			@org = Organisation.find params[:id]
			@subtitle = @org.name
			@location = @org.location
			@registrant = @org.registrant
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_organisations_path, flash:
				{error: "Could not find organisation"}
		end
	end

	def edit
		begin
			@org = Organisation.find params[:id]
			raise IOError.new if @org.can_edit?(current_user) == false
			@subtitle = "Editing #{@org.name}"

			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_organisations_path,
				flash: {error: "Could not find organisation"}
		end
	end

	def update
		begin
			@org = Organisation.find params[:id]

			@org.type = params[:organisation][:type]
			@org.name = params[:organisation][:name]
			@org.registrant_id = params[:organisation][:registrant_id]
			@org.public = params[:organisation][:public]
			@org.descr = params[:organisation][:descr]
			@org.location_id = params[:organisation][:location_id]

			@org.save!

			redirect_to console_organisation_path(@org),
				flash: {success: "Organisation updated"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_organisations_path,
				flash: {error: "Could not find organisation"}
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Editing #{@org.name}"
			@locations = Location.all.order(:name)
			@users = User.all.order(:user_name)
			flash.now[:error] = "Invalid parameters"
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		begin
			@org = Organisation.find params[:id]

			@org.destroy!

			redirect_to console_organisations_path, flash: {success: "Organisation deleted"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to console_organisations_path, flash: {error: "Could not find organisation"}
		rescue ActiveRecord::RecordNotDestroyed
			@subtitle = @org.name
			flash.now[:error] = "Could not delete organisation"
			render :show, status: :forbidden
		end
	end
end

end

# vim: ts=4 sts=4 sw=0 noexpandtab
