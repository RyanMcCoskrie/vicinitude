require 'securerandom'


class HomeController < ApplicationController
	before_action :logged_in_check
	before_action :load_map_source, only: :index


	def index
		@subtitle = "Home"
		if current_user.home_location
			@lat = current_user.home_location.lat
			@lng = current_user.home_location.lng
		else
			@lat = Rails.configuration.vicinitude[:map_centre][:lat]
			@lng = Rails.configuration.vicinitude[:map_centre][:lng]
		end
		@high_zoom = Rails.configuration.vicinitude[:map_centre][:high_zoom]
		@low_zoom = Rails.configuration.vicinitude[:map_centre][:low_zoom]

		#Add all Neighbourhoods to the map
		@events = {}
		Location.where(ancestry: nil).each do |hood|
			@events[hood] = []
		end

		#Associate events with the neighbourhoods they are in
		now = DateTime.now
		Event.where('end_time > ?', now).each do |event|
			@events[event.location.root] << event
		end
	end

	def profile
		@user = current_user
		@subtitle = "Your profile"

		@loc_subs = Subscription.where(
			user: current_user,
			subscribable_type: "Location"
		)
		@org_subs = Subscription.where(
			user: current_user,
			subscribable_type: "Organisation"
		)
	end

	def edit_profile
		@user = current_user
		@subtitle = "Editing your profile"
		@locations = Location.where(ancestry: nil)
	end

	def update_profile
		begin
			raise BadConfirmation if params[:confirmation] != params[:password]

			@user = current_user()

			@user.real_name = params[:real_name]
			@user.email = params[:email]
			if params[:password].empty? == false
				@user.password = params[:password]
			end
			if params[:home_location_id].blank? == false
				@user.home_location = Location.find params[:home_location_id]
			else
				@user.home_location = nil
			end
			@user.save!
			redirect_to profile_path, flash: {success: "Profile updated"}
		rescue BadConfirmation => e
			@subtitle = "Editing your profile"
			flash.now[:error] = "Bad password confirmation"
			@locations = Location.where(ancestry: nil)
			render :edit_profile, status: :unprocessable_entity
		rescue ActiveRecord::RecordNotFound => e
			@subtitle = "Editing your profile"
			flash.now[:error] = "Home location not found"
			@locations = Location.where(ancestry: nil)
			render :edit_profile, status: :unprocessable_entity
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Editing your profile"
			flash.now[:error] = "Invalid Parameters"
			@locations = Location.where(ancestry: nil)
			render :edit_profile, status: :unprocessable_entity
		end
	end

	def request_verification
		address = params[:address] #Snail-Mail

		begin
			throw AlreadyVerified if current_user.verification_code

			@user = current_user()

			@user.verification_code = SecureRandom.hex(4)
			@user.save!

			UserMailer.verification_request(
				@user.user_name,
				@user.real_name,
				@user.verification_code,
				address
			).deliver_later

			redirect_to profile_path, flash: {success: "Request sent"}
		rescue AlreadyVerified => e
			redirect_to profile_path, status: :forbidden,
				flash: {error: "You have already requested to be verified"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to profile_path, status: :internal_server_error,
				flash: {error: "Could not save verification code"}
		end
	end

	def confirm_verification
		@user = current_user()

		begin
			raise BadConfirmation if params[:code] != @user.verification_code
			raise AlreadyVerified if @user.verified?

			priv = @user.priv
			@user.priv = priv | 0x01
			@user.save!
			redirect_to profile_path, flash: {success: "You are now verified!"}
		rescue BadConfirmation => e
			redirect_to profile_path, flash: {error: "Incorrect code"}
		rescue AlreadyVerified => e
			redirect_to profile_path, flash: {error: "You are already verified"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to profile_path,
				flash: {error: "Could not save verified status"}
		end
	end

	def delete_profile
		if current_user.id == 1
			redirect_to profile_path, status: :forbidden,
				flash: {error: "Root user can not be deleted"}
		else
			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'user', action: 'delete'
			}).save

			current_user.destroy

			session[:user_id] = nil
			redirect_to root_path,
				flash: {success: "Your account has been deleted"}
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
