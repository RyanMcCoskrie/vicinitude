class MembershipsController < ApplicationController
	layout "special"

	def accept_application
		begin
			membership = Membership.find_signed! params[:sgid]
			membership.priv = 2
			membership.save!

			OrganisationMailer.application_approved(
				membership.member,
				membership.organisation
			).deliver_now

			flash.now[:success] = "User accepted"
		rescue ActiveRecord::RecordNotFound => e
			flash.now[:error] = "Invaid or expired link"
			render status: :unprocessable_entity
		end
	end

	def reject_application
		begin
			membership = Membership.find_signed! params[:sgid]
			applicant = membership.member
			organisation = membership.organisation

			membership.destroy!

			OrganisationMailer.application_rejected(
				applicant,
				organisation
			).deliver_now

			flash.now[:success] = "User rejected"
		rescue ActiveRecord::RecordNotFound => e
			flash.now[:error] = "Invaid or expired link"
			render status: :unprocessable_entity
		end
	end

	def accept_invitation
		begin
			org = Organisation.find_signed! params[:org_sgid]
			user = User.find_signed! params[:user_sgid]

			membership = Membership.find_by(organisation: org, member: user)
			if membership != nil
				raise AlreadyMember.new
			else
				membership = Membership.create(organisation: org, member: user)
				membership.save!
			end

			OrganisationMailer.invitation_accepted(org, user).deliver_now

			flash.now[:success] = "You have joined #{org.name}"
		rescue ActiveRecord::RecordNotFound => e
			flash.now[:error] = "Invaid or expired link"
			render status: :unprocessable_entity
		rescue AlreadyMember => e
			flash.now[:error] = "You are already a member"
			render status: :unprocessable_entity
		end
	end

	def reject_invitation
		begin
			org = Organisation.find_signed! params[:org_sgid]
			user = User.find_signed! params[:user_sgid]

			OrganisationMailer.invitation_rejected(org, user).deliver_now

			#Set flash message

			flash.now[:success] = "You have declined joining #{org.name}"
		rescue ActiveRecord::RecordNotFound => e
			flash.now[:error] = "Invaid or expired link"
			render status: :unprocessable_entity
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
