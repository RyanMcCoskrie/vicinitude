require "active_support/concern"

module EventsConcern
	extend ActiveSupport::Concern

    included do
		def default_start_time
			return Time.zone.now.at_noon + 1.day
		end

		def default_end_time
			return Time.zone.now.at_noon + 1.day + 2.hours
		end

		def parse_time(str)
			return Time.zone.strptime(str, '%F %R').utc
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
