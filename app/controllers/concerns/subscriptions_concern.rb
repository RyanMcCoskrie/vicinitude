require "active_support/concern"

module SubscriptionsConcern
	extend ActiveSupport::Concern
	include ExceptionHelper

	included do
		def perform_subscription(subscribable)
			sub = Subscription.create(
					user: current_user,
					subscribable: subscribable
			)
			sub.save!
		end

		def perform_unsubscription(subscribable)
			sub = Subscription.find_by(
					user: current_user,
					subscribable: subscribable
			)

			if sub != nil
				sub.destroy!
			else
				raise AlreadyUnsubscribed.new
			end
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
