require "active_support/concern"

module CommentsConcern
	extend ActiveSupport::Concern

	included do
		def perform_create_comment(parent, body)
			comment = Comment.new
			comment.commentable = parent #via set_commentable
			comment.user_id = current_user.id
			comment.body = body
			comment.time = Time.now

			comment.save!

			ActionLog.create(json: {
				user_name: current_user.user_name,
				user_id: current_user.id,
				model: 'comments', action: 'create',
				parent_type: comment.commentable_type,
				parent_name: parent.name,
				parent_id: parent.id,
				entry_body: comment.body
			}).save
		end

		def perform_delete_comment(id)
			comment = Comment.find id

			raise IOError.new if comment.can_delete?(current_user) == false

			comment.destroy!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'comments', action: 'delete',
				parent_type: comment.commentable_type,
				parent_name: comment.commentable.name,
				parent_id: comment.commentable.id,
				entry_id: comment.id, entry_body: comment.body
			}).save
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
