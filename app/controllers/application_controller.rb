class ApplicationController < ActionController::Base
	include ApplicationHelper
	include ExceptionHelper
	include Pagy::Backend
end

# vim: ts=4 sts=4 sw=0 noexpandtab
