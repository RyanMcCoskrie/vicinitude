class FeedbackController < ApplicationController
	before_action :logged_in_check

	def index
		@subtitle = "Send feedback"
	end

	def general
		@subtitle = "Feedback"
	end

	def submit_general
		FeedbackMailer.general_feedback(
			current_user.real_name,
			current_user.user_name,
			params[:subject],
			params[:body]
		).deliver_later

		redirect_to home_path, flash: {success: "Your message has been sent"}
	end

	def location
		@subtitle = "Request a location"
	end

	def submit_location
		FeedbackMailer.request_location(
			current_user.real_name,
			current_user.user_name,
			params[:name],
			params[:body]
		).deliver_later

		redirect_to home_path, flash: {success: "Your message has been sent"}
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
