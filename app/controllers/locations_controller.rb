class LocationsController < ApplicationController
	include SubscriptionsConcern
	before_action :logged_in_check
	before_action :load_map_source, only: :show


	def index
		@subtitle = "Locations"
		@locations = Location.where(ancestry: nil).order(:name)
	end

	def show
		begin
			@location = Location.find_by! name: params[:name]
			@subtitle = @location.name
			@zoom = Rails.configuration.vicinitude[:map_centre][:low_zoom]

			now = Time.now
			@events = Event.where(
				["location_id = ? and start_time > ?",
				@location.id, now]
			)
			@organisations = @location.all_organisations
		rescue ActiveRecord::RecordNotFound => e
			redirect_to locations_path,
				flash: {error: "Could not find location"}
		end
	end

	def subscribe
		begin
			location = Location.find_by! name: params[:location_name]
			user = current_user()

			if user.subscriptions.find_by(subscribable: location)
				raise AlreadySubscribed
			else
				#Check if user is subscribed indirectly
				location.ancestors.each do |ancestor|
					if user.subscriptions.find_by(subscribable: ancestor)
						location = ancestor
						raise AlreadySubscribed
					end
				end

				perform_subscription(location)

				redirect_to location_path(location.name), flash:
					{success: "Subscribed to #{location.name}"}
			end
		rescue ActiveRecord::RecordNotFound => e
			redirect_to locations_path, flash:
				{error: "Could not find location"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to location_path(location), flash:
				{error: "Could not save subscription"}
		rescue AlreadySubscribed => e
			redirect_to location_path(location.name), flash:
				{error: "You are already subscribed to #{location.name}"}
		end
	end

	def unsubscribe
		begin
			location = Location.find_by! name: params[:location_name]

			perform_unsubscription(location)

			redirect_to location_path(location.name), flash:
				{success: "Unsubscribed from #{location.name}"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to locations_path, flash:
				{error: "Could not find location"}
		rescue AlreadyUnsubscribed => e
			redirect_to location_path(location.name), flash:
				{info: "You are not subscribed to #{location.name}"}
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
