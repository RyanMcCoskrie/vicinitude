class SessionController < ApplicationController
	include UserHelper

	layout "special", only: [:password_reset_request]
	layout "meta"
	before_action :logged_out_check
	skip_before_action :logged_out_check, only: [:logout]


	def signup
		@subtitle = "Sign Up"
		@user = User.new
		@locations = Location.where(ancestry:nil).order(:name)
	end

	def register
		begin
			@user = User.new
			@user.user_name = params[:user_name]
			@user.real_name = params[:real_name]
			@user.email = params[:email]
			@user.password = params[:password]

			if params[:home_location_id].blank? == false
				@user.home_location = Location.find( params[:home_location_id] )
			end
			@user.priv = 0

			raise DuplicateUser if User.find_by_user_name(params[:user_name])
			raise InvalidPassword if is_invalid_password(params[:password])
			raise BadConfirmation if params[:confirmation] != params[:password]

			@user.save!
			session[:user_id] = @user.id

			ActionLog.create(json: {
				user_name: @user.user_name, user_id: @user.id,
				model: 'user', action: 'create'
			}).save

			redirect_to home_path, flash: {success: "Account created"}
		rescue DuplicateUser => e
			@subtitle = "Sign Up"
			@locations = Location.where(ancestry:nil).order(:name)
			flash.now[:error] = "Account already exist"
			render :signup, status: :forbidden
		rescue InvalidPassword => e
			@subtitle = "Sign Up"
			@locations = Location.where(ancestry:nil).order(:name)
			flash.now[:error] = "Invalid password"
			render :signup, status: :unprocessable_entity
		rescue BadConfirmation => e
			@subtitle = "Sign Up"
			@locations = Location.where(ancestry:nil).order(:name)
			flash.now[:error] = "Bad password confirmation"
			render :signup, status: :unprocessable_entity
		rescue ActiveRecord::RecordNotFound => e
			@subtitle = "Sign Up"
			@locations = Location.where(ancestry:nil).order(:name)
			flash.now[:error] = "Home Location not found"
			render :signup, status: :unprocessable_entity
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Sign Up"
			@locations = Location.where(ancestry:nil).order(:name)
			flash.now[:error] = "Could not create account"
			render :signup, status: :unprocessable_entity
		end
	end

	def login
		@subtitle = "Login"
		@user = User.new
	end

	def authenticate
		begin
			@user = User.find_by_user_name! params[:user_name]

			raise InvalidPassword if @user.authenticate(params[:password]) == false

			session[:user_id] = @user.id
			redirect_to home_path, flash: {success: "Successfully logged in"}
		rescue ActiveRecord::RecordNotFound => e
			@subtitle = "Login"
			flash.now[:error] = "Invalid user name or password"
			render :login, status: :forbidden
		rescue InvalidPassword => e
			@subtitle = "Login"
			flash.now[:error] = "Invalid user name or password"
			render :login, status: :forbidden
		end
	end

	def logout
		session[:user_id] = nil
		redirect_to login_path, flash: {success: "Logged out. See you later!"}
	end

	def forgot_password
		@subtitle = "Forgot Password"
	end

	def password_reset_request
		@subtitle = "Forgot Password"

		user = User.find_by_email(params[:email])

		if user != nil
			sgid = user.signed_id(expires_in: 24.hours, purpose: :password_reset)

			UserMailer.password_reset_request(user, sgid).deliver_now
		end

		redirect_to password_reset_sent_path, flash:
			{info: "You will receive an email containing a reset link IF you entered a valid address"}
	end

	def password_reset
		@subtitle = "Reset Password"
	end

	def submit_password_reset
		@subtitle = "Reset Password"

		begin
			raise InvalidPassword if is_invalid_password(params[:password])
			raise BadConfirmation if params[:confirmation] != params[:password]

			@user = User.find_signed! params[:sgid], purpose: :password_reset

			@user.password = params[:password]
			@user.save!

			redirect_to login_path, flash:
				{success: "Your password has been updated"}
		rescue InvalidPassword => e
			redirect_to password_reset_path(params[:sgid]), flash:
				{error: "Invalid password"}
		rescue BadConfirmation => e
			redirect_to password_reset_path(params[:sgid]), flash:
				{error: "Password and confirmation do not match"}
		rescue ActiveSupport::MessageVerifier::InvalidSignature => e
			redirect_to password_reset_path(params[:sgid]), flash:
				{error: "Could not find password-reset code"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to password_reset_path(params[:sgid]), flash:
				{error: "Could not find user account"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to password_reset_path(params[:sgid]), flash:
				{error: "Could not update password"}
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
