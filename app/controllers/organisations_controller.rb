class OrganisationsController < ApplicationController
	include SubscriptionsConcern
	before_action :logged_in_check, except: [:accept_transfer, :reject_transfer]
	layout "special", only: [:accept_transfer, :reject_transfer]


	def index
		@subtitle = "Organisations"

		@faiths = Faith.order(:name)
		@govtdepts = GovtDept.order(:name)
		@charities = Charity.order(:name)
		@companies = Company.order(:name)
		@clubs = Club.order(:name)
	end

	def new
		@org = Organisation.new
		@locations = Location.all.order(:name)
		@subtitle = "Create Organisation"
	end

	def create
		begin
			@org = Organisation.new

			@org.name = params[:organisation][:name]
			@org.type = params[:organisation][:type]
			@org.public = params[:organisation][:public]
			@org.descr = params[:organisation][:descr]
			@org.registrant_id = current_user.id
			@org.location_id = params[:organisation][:location_id]

			@org.save!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'organisation', action: 'create',
				entry_id: @org.id, entry_name: @org.name, entry_type: @org.type
			}).save

			redirect_to organisation_path(@org.name),
				flash: {success: "Organisation Created"}
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Create Organisation"
			flash.now[:error] = "Invalid parameters"
			render :new, status: :unprocessable_entity
		end
	end

	def show
		begin
			@org = Organisation.find_sole_by name: params[:name]
			@subtitle = @org.name
			@discussions = @org.discussions.order("id desc").limit(5)
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation"}
		end
	end

	def edit
		begin
			@org = Organisation.find_sole_by name: params[:name]
			raise IOError.new if @org.can_edit?(current_user) == false
			@subtitle = "Editing #{@org.name}"

			@locations = Location.all.order(:name)
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path,
				flash: {error: "Could not find organisation"}
		end
	end

	def update
		begin
			@org = Organisation.find_sole_by name: params[:name]

			raise IOError.new if @org.can_edit?(current_user) == false
			@org.type = params[:organisation][:type]
			@org.name = params[:organisation][:name]
			@org.public = params[:organisation][:public]
			@org.descr = params[:organisation][:descr]
			@org.location_id = params[:organisation][:location_id]

			@org.save!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'organisation', action: 'update',
				entry_name: @org.name, entry_type: @org.type, entry_id: @org.id
			}).save

			redirect_to organisation_path(@org.becomes(Organisation).name),
				flash: {success: "Organisation updated"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path,
				flash: {error: "Could not find organisation"}
		rescue IOError => e
			redirect_to organisation_path(@org.name),
				flash: {error: "Insufficiant permissions"}
		rescue ActiveRecord::RecordInvalid => e
			@subtitle = "Editing #{@org.name}"
			flash.now[:error] = "Invalid parameters"
			render :edit, status: :unprocessable_entity
		end
	end

	def destroy
		begin
			@org = Organisation.find_sole_by name: params[:name]
			raise IOError.new if @org.can_edit?(current_user) == false

			@org.destroy!

			ActionLog.create(json: {
				user_name: current_user.user_name, user_id: current_user.id,
				model: 'organisation', action: 'delete',
				entry_name: @org.name, entry_type: @org.type, entry_id: @org.id
			}).save

			redirect_to organisations_path,
				flash: {success: "Organisation deleted"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path,
				flash: {error: "Could not find organisation"}
		rescue IOError => e
			@subtitle = @org.name
			flash.now[:error] = "Insufficient permissions"
			render :show, status: :internal_server_error
		rescue ActiveRecord::RecordNotDestroyed
			@subtitle = @org.name
			flash.now[:error] = "Could not delete organisation"
			render :show, status: :forbidden
		end
	end

	# XXX Hence-forward the parameter is :organisation_name, blame rails
	def choose_moderators
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]
			@members = @org.members.reject{|m| m == @org.registrant}
			raise IOError.new if @org.registrant != current_user

			@subtitle = "Choose moderators for #{@org.name}"
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation"}
		rescue IOError => e
			redirect_to organisation_path(@org.name), flash:
				{error: "Insufficient permissions"}
		end
	end

	def update_moderators
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]
			raise IOError.new if @org.registrant != current_user

			params[:users].each do |pair|
				membership = Membership.find_sole_by(
					organisation: @org,
					member_id: pair[0]
				)

				if pair[1] == "1"
					membership.priv = 0x06
				else
					membership.priv = 0x02
				end

				membership.save!
			end

			redirect_to organisation_path(@org.name), flash:
				{success: "Moderators updated"}
		rescue ActiveRecord::RecordNotFound => e
			puts "\n\n\n#{e}\n\n\n"
			redirect_to organisations_path, flash:
				{error: "Could not find organisation, user or membership"}
		rescue IOError => e
			redirect_to organisation_path(@org.name), flash:
				{error: "Insufficient permissions"}
		end
	end

	def apply
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]

			#Check if already a member
			membership = Membership.find_by(
				member_id: current_user.id,
				organisation_id: @org.id
			)

			if membership != nil
				if membership.priv = 1
					raise AlreadyApplied.new
				else
					raise AlreadyMember.new
				end
			else
				membership = Membership.create(
					member: current_user,
					organisation: @org,
					priv: 1
				)
				membership.save
				sgid = membership.signed_id(expires_in: 7.days)

				OrganisationMailer.new_applicant(
					@org,
					current_user,
					sgid
				).deliver_now

				redirect_to organisation_path(@org.name), flash:
					{success: "Your application has been sent"}
			end
		rescue AlreadyApplied => e
			redirect_to organisation_path(params[:organisation_name]), flash:
				{error: "You have already applied"}
		rescue AlreadyMember => e
			redirect_to organisation_path(params[:organisation_name]), flash:
				{error: "You are already a member"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation"}
		end
	end

	# XXX parameter is :organisation_name, blame rails
	def leave
		begin
			@org = Organisation.find_by name: params[:organisation_name]
			membership = Membership.find_sole_by(
							organisation: @org,
							member: current_user
			)
			if @org.registrant != current_user
				membership.destroy
				redirect_to organisation_path(@org.name), flash:
					{success: "You have left #{@org.name}"}
			else
				raise IOError.new #Registrant must transfer ownership first
			end
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation or membership record"}
		rescue ActiveRecord::RecordNotDestroyed => e
			redirect_to organisation_path(@org.name), flash:
				{error: "Could not delete membership"}
		rescue IOError.new => e
			redirect_to organisation_path(@org.name), flash:
				{error: "You must transfer ownership before leaving"}
		end
	end

	def subscribe
		begin
			org = Organisation.find_by_name params[:organisation_name]

			perform_subscription(org)

			redirect_to organisation_path(org.name), flash:
				{success: "Subscribed to #{org.name}"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation"}
		rescue AlreadySubscribed => e
			redirect_to organisation_path(params[:organisation_name]), flash:
				{error: "You are already subscribed"}
		rescue ActiveRecord::RecordInvalid => e
			redirect_to organisation_path(params[:organisation_name]), flash:
				{error: "Could not save subscription"}
		end
	end

	def unsubscribe
		begin
			org = Organisation.find_by_name params[:organisation_name]

			perform_unsubscription(org)

			redirect_to organisation_path(org.name), flash:
				{success: "Unsubscribed from #{org.name}"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation"}
		rescue AlreadyUnsubscribed => e
			redirect_to organisation_path(params[:organisation_name]), flash:
				{error: "You are already unsubscribed"}
		rescue ActiveRecord::RecordNotDestroyed => e
			redirect_to organisation_path(params[:organisation_name]),
				{error: "Could not delete subscription"}
		end
	end

	def invite_members
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]
			@subtitle = "Invite users to join #{@org.name}"

			@users = User.all.reject{|u| @org.members.include? u}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation"}
		end
	end

	def send_invitations
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]

			#For some reason the multi-select sends blank entries
			ids = params[:users].reject{|id| id.blank?}

			ids.each do |id|
				user = User.find id
				OrganisationMailer.invite_user(@org, user).deliver_later!
			end

			redirect_to organisation_path(@org.name), flash:
				{success: "Invitations have been sent"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation or one of the users"}
		end
	end

	def transfer
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]
			@subtitle = "Transfer registration of #{@org.name}"

			@members = @org.members
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation"}
		end
	end

	def begin_transfer
		begin
			@org = Organisation.find_sole_by name: params[:organisation_name]
			@user = User.find params[:user_id]

			@org_sgid = @org.signed_id(expires_in: 7.days)
			@user_sgid = @user.signed_id(expires_in: 7.days)

			OrganisationMailer.transfer_offered(
				@org,
				@user,
				@org_sgid,
				@user_sgid
			).deliver_now!

			redirect_to organisation_path(@org.name), flash:
				{success: "Registration transfer initiated"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation or user"}
		end
	end

	def accept_transfer
		begin
			@org = Organisation.find_signed! params[:org_sgid]
			@old_registrant = @org.registrant
			@user = User.find_signed! params[:user_sgid]

			@org.registrant = @user
			@org.save

			OrganisationMailer.transfer_accepted(
				@org,
				@old_registrant,
				@user
			).deliver_now!

			flash.now[:success] = "You have accepted registration of #{@org.name}"
		rescue ActiveSupport::MessageVerifier::InvalidSignature => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation or user"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation or user"}
		end
	end

	def reject_transfer
		begin
			@org = Organisation.find_signed! params[:org_sgid]
			@user = User.find_signed! params[:user_sgid]

			OrganisationMailer.transfer_rejected(@org, @user).deliver_now!

			flash.now[:success] = "You have rejected registration of #{@org.name}"
		rescue ActiveSupport::MessageVerifier::InvalidSignature => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation or user"}
		rescue ActiveRecord::RecordNotFound => e
			redirect_to organisations_path, flash:
				{error: "Could not find organisation or user"}
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
