class Discussion < ApplicationRecord
	validates :name, presence: true
	validates :time, presence: true

	belongs_to :poster, class_name: :User
	belongs_to :organisation
	has_many :comments, dependent: :destroy, as: :commentable

	def can_edit?(user)
		if user.admin? or user.moderator?
			return true
		elsif self.poster == user
			return true
		else
			return false
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
