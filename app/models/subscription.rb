class Subscription < ApplicationRecord
	validates :user, presence: true
	validates :subscribable, presence: true

	belongs_to :subscribable, polymorphic: true
	belongs_to :user
end

# vim: ts=4 sts=4 sw=0 noexpandtab
