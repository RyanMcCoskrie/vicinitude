class Event < ApplicationRecord
	validates :name, presence: true
	validates :start_time, presence: true
	validates :end_time, presence: true, comparison: {greater_than: :start_time}

	belongs_to :user
	belongs_to :location
	belongs_to :organisation, optional: true
	has_many :comments, dependent: :destroy, as: :commentable

	def can_edit?(user)
		if user.admin?
			return true
		elsif user.moderator?
			return true
		elsif self.user == user
			return true
		else
			return false
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
