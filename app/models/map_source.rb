class MapSource < ApplicationRecord
	validates :name, presence: true, uniqueness: true
	validates :url, presence: true
    validates :is_vector, inclusion: [true, false]
end

# vim: ts=4 sts=4 sw=0 noexpandtab

