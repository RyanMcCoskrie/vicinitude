class Location < ApplicationRecord
	has_ancestry
	serialize :breadcrumbs, class: Array
	before_save :set_breadcrumbs
	before_destroy :free_users

	validates :name, presence: true, uniqueness: true
	validates :lat, presence: true
	validates :lng, presence: true

	has_many :events, dependent: :destroy, class_name: :Event
	has_many :organisations, dependent: :destroy

	has_many :subscriptions, as: :subscribable

	def all_subscribers
		list = self.subscriptions

		self.ancestors.each{|ancestor| list = list + ancestor.subscriptions}

		return list.map{|sub|sub.user}.uniq
	end

	def all_organisations
		list = self.organisations

		self.descendants{|loc| list = list + loc.organisations}

		return list
	end

	private
	def set_breadcrumbs
		self.breadcrumbs = []
		self.ancestors.each {|ancestor| self.breadcrumbs << ancestor.name}
	end

	def free_users
		users = User.where(home_location_id: self.id).update(home_location_id: nil)
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
