class Organisation < ApplicationRecord
	validates :name, presence: true
	validates :registrant, presence: true
	validates_inclusion_of :public, in: [true, false]

	belongs_to :location
	belongs_to :registrant, class_name: :User, foreign_key: :registrant_id
	has_many :memberships, dependent: :destroy
	has_many :members, class_name: :User, through: :memberships
	has_many :subscriptions, dependent: :destroy, foreign_key: :subscribable
	has_many :subscribers, class_name: :User, through: :subscriptions
	has_many :discussions, dependent: :destroy
	has_many :events, dependent: :destroy

	after_save_commit :add_registrant_to_members

	def member?(user)
		if Membership.find_by(organisation: self, member: user)
			return true
		else
			return false
		end
	end

	def subscriber?(user)
		if Subscription.find_by(subscribable: self, user: user)
			return true
		else
			return false
		end
	end

	def can_edit?(user)
		m = Membership.find_by(organisation: self, member: user)
		if user.moderator? or user.admin? #Site-wide editing powers
			return true
		elsif m != nil
			if user == self.registrant or m.moderator?
				return true
			else
				return false
			end
		else
			return false #Dismiss regular non-members
		end
	end

	private
	def add_registrant_to_members
		if self.members.include?(self.registrant) == false
			self.members << self.registrant
			membership = Membership.find_sole_by(member: self.registrant,
						organisation: self)
			membership.priv = 0x06
			membership.save
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
