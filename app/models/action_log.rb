require 'json/add/ostruct'

class ActionLog < ApplicationRecord
	def json
		return JSON.parse(self.data, object_class: OpenStruct)
	end

	def json=(hash)
		self.data = hash.to_json
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
