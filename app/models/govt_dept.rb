class GovtDept < Organisation
	before_validation :set_public_to_false

	def public
		false
	end

	private
	def set_public_to_false
		self.public = false
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
