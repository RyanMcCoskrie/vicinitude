require 'bcrypt'

class User < ApplicationRecord
	validates :user_name, presence: true, uniqueness: true
	validates :real_name, presence: true
	validates :email, presence: true, uniqueness: true, email: true
	validates :password_salt, presence: true
	validates :password_hash, presence: true
	validates :priv, presence: true

	belongs_to :home_location, class_name: :Location, foreign_key: :home_location_id, optional: true
	has_many :events, dependent: :destroy
	has_many :comments, class_name: :Comment, dependent: :destroy
	has_many :subscriptions, dependent: :destroy
	has_many :memberships, foreign_key: :member_id, dependent: :destroy
	has_many :registrations, class_name: :Organisation, foreign_key: :registrant_id

	#XXX Figure out how to do this as an assocation
	def organisations
		return self.memberships.map{|m| m.organisation}
	end

	def password=(password)
		salt = BCrypt::Engine.generate_salt
		hash = BCrypt::Engine.hash_secret(password, salt)
		self.password_salt = salt
		self.password_hash = hash
	end

	def authenticate(password)
		tmp = BCrypt::Engine.hash_secret(password, self.password_salt)
		return tmp == self.password_hash
	end

	def verified?
		self.priv & 0x01 == 0x01
	end

	def suspended?
		self.priv & 0x02 == 0x02
	end

	def moderator?
		self.priv & 0x04 == 0x04
	end

	def admin?
		self.priv & 0x08 == 0x08
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
