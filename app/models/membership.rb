class Membership < ApplicationRecord
	validates :priv, presence: true

	belongs_to :member, class_name: :User
	belongs_to :organisation

	before_validation :add_priv_field

	#Privilege bit-positions
	# 0x01 Applicant
	# 0x02 Member
	# 0x04 Moderator

	def applicant?
		self.priv & 0x01 == 0x01
	end

	def member?
		self.priv & 0x02 == 0x02
	end

	def moderator?
		self.priv & 0x04 == 0x04
	end

private
	def add_priv_field
		self.priv ||= 0x02 #Default to full member for now
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
