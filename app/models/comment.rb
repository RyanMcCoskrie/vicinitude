class Comment < ApplicationRecord
	validates :time, presence: true
	validates :body, presence: true

	belongs_to :commentable, polymorphic: true
	belongs_to :user

	def can_delete?(user)
		if user.admin?
			return true
		elsif user.moderator?
			return true
		elsif self.user == user
			return true
		else
			return false
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
