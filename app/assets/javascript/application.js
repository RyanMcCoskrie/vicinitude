import $ from "jquery";
export {default as $} from "jquery";

import "leaflet";
import "@maplibre/maplibre-gl-leaflet";
import flatpickr from "flatpickr/dist/esm";
import Trix from "trix";

/////////////////////////
//    Map Functions    //
/////////////////////////
export function useMap(func)
{
	try {
		$('#map').ready(func);
	}
	catch (error) {
		$("#map").css("background", "silver");
		$("#map").css("font-weight", "bold");
		$("#map").text("Error: Could not load map");
	}
}

export function initMap(id, src, attr, isVector, lat, lng, zoom)
{
	var tileLayer = null;
	var centre = new L.LatLng(lat, lng);
	var map = new L.Map(id, {
		center: centre,
		zoom: zoom,
		attributionControl: true,
		closePopupOnClick: false
	});
	//By default Leaflet endorses a Nation-State, remove this behaviour
	map.attributionControl.setPrefix(false);
	if(attr.length != 0) {
		map.attributionControl.addAttribution(attr);
	}
	map.attributionControl.addAttribution('&copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a>');
	map.attributionControl.addAttribution('&copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>');

	if(isVector){
		tileLayer = new L.maplibreGL({style: src});
	} else {
		tileLayer = new L.tileLayer(src, {maxZoom: 19});
	}

	map.addLayer(tileLayer);
	window.map = map;
}

export function makeMarker(lat, lng, str)
{
	var marker = L.marker([lat, lng]).addTo(window.map);
	marker.bindPopup(str, {autoClose: false}).openPopup();
};

////////////////////////////
//    Social Functions    //
////////////////////////////
export function postComment()
{
	var id = $('input[name=id]')[0].value;

	$.ajax(`/events/${id}/comments`, {
		method: 'POST',
		data: {
			body: $('textarea')[0].value
		},
		dataType: "html",
		success: function(data, status, jqXHR) {
			$('#comment_list').append(data);
			$('textarea')[0].value = '';
		},
		error: function(jqXHR, status, msg) {
			alert(`Something went wrong: ${msg}`);
		}
	});
};

//////////////////////////////
//    Calendar Functions    //
//////////////////////////////
export function initEventCalendars()
{
	flatpickr("#start_time", {
		enableTime: true,
		dateFormat: "Y-m-d H:i",
		time_24hr: true,
		minuteIncrement: 5
	});
	flatpickr("#end_time", {
		enableTime: true,
		dateFormat: "Y-m-d H:i",
		time_24hr: true,
		minuteIncrement: 5
	});
};

//////////////////////////////////////
//    Rich-Text Editor Functions    //
//////////////////////////////////////
export function initRichTextEditor()
{
	Trix.config.dompurify.ALLOWED_TAGS = [
		'#text', 'div', 'p', 'br', 'em', 'strong', 'del', 'h1','ul', 'ol', 'li'
	];
	Trix.config.dompurify.ALLOWED_ATTRS = [ ];

	const {lang} = Trix.config;
	$(document).ready(function(){
		document.getElementById("trix-toolbar-1").innerHTML =
`<div class="trix-button-row">
	<span class="trix-button-group trix-button-group--text-tools"
			data-trix-button-group="text-tools">
		<button type="button" class="trix-button trix-button--icon trix-button--icon-bold"
				data-trix-attribute="bold" data-trix-key="b"
				title="${lang.bold}" tabindex="-1">
			${lang.bold}
		</button>
		<button type="button" class="trix-button trix-button--icon trix-button--icon-italic"
				data-trix-attribute="italic" data-trix-key="b"
				title="${lang.italic}" tabindex="-1">
			${lang.italic}
		</button>
		<button type="button" class="trix-button trix-button--icon trix-button--icon-strike"
				data-trix-attribute="strike" data-trix-key="b"
				title="${lang.strike}" tabindex="-1">
			${lang.strike}
		</button>
		<button type="button" class="trix-button trix-button--icon trix-button--icon-heading-1"
				data-trix-attribute="heading1" title="${lang.heading1}"
				tabindex="-1">
			${lang.heading1}
		</button>
		<button type="button" class="trix-button trix-button--icon trix-button--icon-bullet-list"
				data-trix-attribute="bullet" title="${lang.bullets}"
				tabindex="-1">
			${lang.bullets}
		</button>
		<button type="button" class="trix-button trix-button--icon trix-button--icon-number-list"
				data-trix-attribute="number" title="${lang.numbers}"
				tabindex="-1">
			${lang.numbers}
		</button>
	</span>
	<span class="trix-button-group-spacer"></span>
	<span class="trix-button-group trix-button-group--history-tools"
			data-trix-button-group="history-tools">
		<button type="button" class="trix-button trix-button--icon trix-button--icon-undo"
				data-trix-action="undo" data-trix-key="z" title="${lang.undo}"
				tabindex="-1">
			${lang.undo}
		</button>
		<button type="button" class="trix-button trix-button--icon trix-button--icon-redo"
				data-trix-action="redo" data-trix-key="shift+z"
				title="${lang.redo}" tabindex="-1">
			${lang.redo}
		</button>
	</span>
</div>`;
	});
}

/////////////////////////////
//    Utility Functions    //
/////////////////////////////
export function alertClose(key)
{
	$(".alert-"+key)[0].style.display = "none";
};

export function toggleSubmit(name)
{
	var submit = $(name)[0];
	if(submit.disabled == true)
		submit.disabled = false;
	else
		submit.disabled = true;
};

// vim: ts=4 sts=4 sw=0 noexpandtab
