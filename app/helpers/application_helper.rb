module ApplicationHelper
	include Pagy::Frontend

	def current_user
		if session.has_key? :user_id
			return User.find session[:user_id]
		else
			return nil
		end
	end

	def logged_in_check
		if current_user == nil
			redirect_to login_path, flash: {error: "You must be logged in"}
		end
	end

	def logged_out_check
		if current_user != nil
			redirect_to home_path, flash: {info: "You are already logged in"}
		end
	end

	def is_admin_check
		unless current_user != nil && current_user.admin?
			redirect_to home_path, flash: {error: "Insufficent permissions"}
		end
	end

	def is_active_path?(path)
		if request.path == path
			return "active"
		else
			return ""
		end
	end

	def load_map_source
		@map_source = MapSource.find(Rails.configuration.vicinitude[:map_source])
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
