module UserHelper
	def is_invalid_password(str)
		str == nil or str.length < 8
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
