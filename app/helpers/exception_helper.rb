module ExceptionHelper
	class BadConfirmation < StandardError; end
	class InvalidPassword < StandardError; end
	class DuplicateUser   < StandardError; end
	class PendingVerification < StandardError; end
	class AlreadyVerified < StandardError; end
	class AlreadySubscribed < StandardError; end
	class AlreadyUnsubscribed < StandardError; end
	class AlreadyMember < StandardError; end
	class AlreadyApplied < StandardError; end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
