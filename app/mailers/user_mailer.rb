class UserMailer < ApplicationMailer
	layout "mailer"

	def verification_request(user_name, real_name, code, address)
		@user_name = user_name
		@real_name = real_name
		@code = code
		@address = address

		mail(
			to: Rails.configuration.action_mailer.smtp_settings[:user_name],
			subject: "Verification request from #{user_name}"
		)
	end

	def password_reset_request(user, sgid)
		@site_name = Rails.configuration.vicinitude[:site_name]
		@real_name = user.real_name
		@user_name = user.user_name
		@email = user.email
		@sgid = sgid

		mail(
			to: user.email,
			subject: "Link for password reset on #{@site_name}"
		)
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
