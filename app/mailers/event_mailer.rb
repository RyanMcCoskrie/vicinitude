class EventMailer < ApplicationMailer
	layout "mailer"

	def upcoming_event(event)
		@event = event
		@location = event.location
		@start_time = @event.start_time.localtime.strftime("%F %R")
		@end_time = @event.end_time.localtime.strftime("%F %R")
		@plain_descr = ActionText::Content.new(event.descr).to_plain_text

		@location.all_subscribers.select{|u| u != @event.user}.each do |user|
			mail(
				to: user.email,
				subject: "Upcoming event #{@event.name} posted on #{@site_name}"
			)
		end
	end

	def updated_event(event)
		@event = event
		@location = event.location
		@start_time = @event.start_time.localtime.strftime("%F %R")
		@end_time = @event.end_time.localtime.strftime("%F %R")
		@plain_descr = ActionText::Content.new(event.descr).to_plain_text

		@location.all_subscribers.select{|u| u != @event.user}.each do |user|
			mail(
				to: user.email,
				subject:"Update regarding #{@event.name} on #{@site_name}"
			)
		end
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
