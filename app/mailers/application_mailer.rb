require "action_mailer"

class ApplicationMailer < ActionMailer::Base
	default from: Rails.configuration.vicinitude[:admin_email]

	layout 'mailer'
	before_action :set_variables

	private
	def set_variables
		@site_name = Rails.configuration.vicinitude[:site_name]
		@host_name = Rails.configuration.action_mailer.default_url_options[:host]
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
