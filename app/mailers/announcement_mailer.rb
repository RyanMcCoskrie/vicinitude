class AnnouncementMailer < ApplicationMailer
	layout "mailer"

	def general_announcement(user, subject, body)
		@subject = subject
		@body = body

		mail(to: user.email, subject: @subject)
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
