class FeedbackMailer < ApplicationMailer
	layout "mailer"

	def general_feedback(real_name, user_name, subject, body)
		@real_name = real_name
		@user_name = user_name
		@subject = subject
		@body = body

		mail(
			to: Rails.configuration.action_mailer.smtp_settings[:user_name],
			subject: "Feedback from #{@real_name} (#{@user_name}): #{@subject}"
		)
	end

	def request_location(real_name, user_name, location_name, body)
		@real_name = real_name
		@user_name = user_name
		@location_name = location_name
		@body = body

		mail(
			to: Rails.configuration.action_mailer.smtp_settings[:user_name],
			subject: "Missing location: #{@location_name}"
		)
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
