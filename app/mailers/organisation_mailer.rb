class OrganisationMailer < ApplicationMailer
	layout "mailer"

	def new_applicant(org, user, sgid)
		@org = org
		@user = user
		@sgid = sgid

		mail(
			to: org.registrant.email,
			subject: "User #{@user.user_name} has applied to join #{@org.name} on #{@site_name}"
		)
	end

	def application_approved(user, org)
		@org = org

		mail(
			to: user.email,
			subject: "Your application on #{@site_name} has been accepted"
		)
	end

	def application_rejected(user, org)
		@org = org

		mail(
			to: user.email,
			subject: "Your application on #{@site_name} has been rejected"
		)
	end

	def invite_user(org, user)
		@org = org
		@org_sgid = @org.signed_id(expires_in: 7.days)
		@user_sgid = user.signed_id(expires_in: 7.days)

		mail(
			to: user.email,
			subject: "You have been invited to join #{@org.name} on #{@site_name}",
			template_name: :invitation
		)
	end

	def invitation_accepted(org, invitee)
		@org = org
		@invitee = invitee

		mail(
			to: @org.registrant.email,
			subject: "An invitation to join #{@org.name} has been accepted"
		)
	end

	def invitation_rejected(org, invitee)
		@org = org
		@invitee = invitee

		mail(
			to: @org.registrant.email,
			subject: "An invitation to join #{@org.name} has been rejected"
		)
	end

	def transfer_offered(org, user, org_sgid, user_sgid)
		@org = org
		@org_sgid = org_sgid
		@user_sgid = user_sgid

		mail(
			to: user.email,
			subject: "You have been offered the registration of #{@org.name}"
		)
	end

	def transfer_accepted(org, old_registrant, new_registrant)
		@org = org
		@new_registrant = new_registrant

		mail(
			to: @new_registrant.email,
			subject: "#{@new_registrant.real_name} has accepted registration of #{@org.name}"
		)
	end

	def transfer_rejected(org, user)
		@org = org
		@user = user

		mail(
			to: @org.registrant.email,
			subject: "#{@user.real_name} has rejected registration of #{@org.name}"
		)
	end
end

# vim: ts=4 sts=4 sw=0 noexpandtab
