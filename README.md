# Vicinitude

### A social media platform for Local Communities

*Why argue with strangers when you can chat with your neighbours?*

## Introduction
Unlike conventional social media platforms (open-source or otherwise)
Vicinitude is designed for small, geographically defined communities rather
than a world-wide userbase. The goal is to on one hand provide platforms that
cater for the specific needs of communities while on the other hand
discouraging local scandals from adding to world-wide tensions.

You are free to deploy a rebranded instance of Vicinitude for your local
region.

## Status
Very early testing. Most features are missing, the visual design is awful and
the developer has only just started researching how to deploy a Ruby-on-Rails
app.

## Dependencies
* Ruby 3.3.4
* Rails 7.2.1
* Ancestry 4.3.3
* PostgreSQL
* Node & Yarn
* JQuery
* Leaflet, MapLibreGL & Stadia Maps
* Boostrap
* Flatpickr

## Deployment
  At present DockerCompose seems to be the most effective way of deploying
  Vicinitude. Be sure to make /config/vicinitude.yml and app/assets/images
  are made into their own Docker volumes that are excluded from Git changes.

  Make sure to read the subsections on Map Tiles, Email and the Database below.

### Time zone
  Be sure to set the TZ value to equal your local timezone. If in doubt please
  consult https://nodatime.org/TimeZones

### Map Tiles

To move from testing on localhost to deploying on a public server you will
need to register your domain with https://stadiamaps.com

NOTE: The Vicinitude project receives no financial benefit from your
registration.

### Email

At present email configuration details are passed through the following
environment variables:
* SMTP_PORT
* SMTP_HOST
* SMTP_USERNAME
* SMTP_PASSWORD
* SMTP_DOMAIN

### Database

Likewise the login details of production databases are passed through the
following environment variables:
* DB_HOST
* DB_NAME
* DB_USERNAME
* DB_PASSWORD

Remember to run **rails db:seed** to create the admin profile with the
temporary password *12345678* and generate the list of available map
sources.

### UI assets

You will also need to install CSS and JS dependencies using the commands
**rails css:build** and **rails javascript:build**

You will need to rewrite config/vicinitude.yml for your specific server,
this can be done through the config page of the admin console.

## Donate

As Ryan McCoskrie is currently the sole developer please send donations to his Ko-Fi
account here: https://ko-fi.com/ryanmccoskrie

Or for ongoing sponsorship donate via LibrePay here: https://liberapay.com/RyanMcCoskrie/
