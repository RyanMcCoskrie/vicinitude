# syntax = docker/dockerfile:1

ARG RUBY_VERSION=3.3.6
FROM registry.docker.com/library/ruby:$RUBY_VERSION-slim AS base1


#Setup environment variables
ENV RAILS_ENV="production"
ENV BUNDLE_DEPLOYMENT="1"
ENV RAILS_SERVE_STATIC_FILES="true"
ENV RAILS_LOG_TO_STDOUT="true"
ENV EDITOR="nano"

#Setup a dedicated user and continue with that account
RUN adduser vicinitude

#Install Debian based dependencies
RUN apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y --no-install-recommends \
		build-essential libpq-dev nodejs npm \
		nano && \
	rm -rf /var/lib/apt/lists/*

#Install NPM globally
RUN npm install -g yarn


FROM base1 AS base2

WORKDIR /opt/vicinitude
RUN chown vicinitude /opt/vicinitude
USER vicinitude:vicinitude

COPY --chown=vicinitude Gemfile Gemfile.lock package.json /opt/vicinitude/

#Install Ruby based dependencies
RUN bundle config --global frozen 1
RUN bundle config --global without development test
RUN bundle install

#Prepare CSS/JS dependencies & assets
RUN yarn install


FROM base2
USER vicinitude:vicinitude
ENV SECRET_KEY_BASE_DUMMY="1"

#Get files from the Vicinitude project
COPY --chown=vicinitude . /opt/vicinitude/

# SECRET_KEY_BASE_DUMMY is not working
RUN bundle exec rails assets:precompile


#Runtime details
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
EXPOSE 3000
