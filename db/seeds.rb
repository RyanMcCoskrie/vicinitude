# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

if User.count == 0
  admin = User.create({user_name: "admin", real_name: "Admin", email: "admin@localhost", priv: 13})
  admin.password = "12345678"
  admin.save!
end

if MapSource.count == 0
  mapSources = [
    MapSource.create(
      name: "Custom",
      url: "Your URL here",
      is_vector: false
    ),
    MapSource.create(
      name: "Outdoors by Stadia Maps (Vector)",
      url: "https://tiles.stadiamaps.com/styles/outdoors.json",
      is_vector: true
    ),
    MapSource.create(
      name: "Stamen Terrain by Stadia Maps",
      url: "https://tiles.stadiamaps.com/tiles/stamen_terrain/{z}/{x}/{y}{r}.png",
      is_vector: false
    ),
    MapSource.create(
      name: "Stamen Terrain by Stadia Maps (Vector)",
      url: "https://tiles.stadiamaps.com/styles/stamen_terrain.json",
      is_vector: true
    )

  ]
  mapSources.each{|source| source.save!}
end
