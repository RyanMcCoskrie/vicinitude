class CreateComments < ActiveRecord::Migration[7.0]
  def change
    create_table :comments do |t|
      t.references :commentable, null: false, polymorphic: true
      t.references :user, null: false, foreign_key: true
      t.datetime :time
      t.text :body

      t.timestamps
    end

    add_index :comments, [:commentable_type, :commentable_id]

    create_join_table :users, :comments do |t|
      t.index [:user_id, :comment_id]
      t.index [:comment_id, :user_id]
    end
  end
end
