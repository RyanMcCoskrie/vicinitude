class AddDefaultLocationsToUsers < ActiveRecord::Migration[7.2]
  def change
    add_reference :users, :home_location, foreign_key: {to_table: :locations}
  end
end
