class CreateSubscriptions < ActiveRecord::Migration[7.0]
  def change
    create_table :subscriptions do |t|
      t.references :subscribable, null: false, polymorphic: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :subscriptions, [:subscribable_type, :subscribable_id]

    create_join_table :users, :subscriptions do |t|
      t.index [:user_id, :subscription_id]
      t.index [:subscription_id, :user_id]
    end
  end
end

