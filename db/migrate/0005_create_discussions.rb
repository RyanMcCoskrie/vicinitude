class CreateDiscussions < ActiveRecord::Migration[7.0]
  def change
    create_table :discussions do |t|
      t.string :name
      t.references :poster, null: false, foreign_key: {to_table: :users}
      t.references :organisation, null: false, foreign_key: true
      t.datetime :time

      t.timestamps
    end
  end
end
