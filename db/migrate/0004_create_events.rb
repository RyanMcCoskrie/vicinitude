class CreateEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :events do |t|
      t.string :name
      t.text :descr
      t.references :user, null: false, foreign_key: true
      t.references :location, null: false, foreign_key: true
      t.references :organisation, foreign_key: true
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end

    create_join_table :locations, :events do |t|
      t.index [:location_id, :event_id]
      t.index [:event_id, :location_id]
    end
  end
end
