class CreateOrganisations < ActiveRecord::Migration[7.0]
  def change
    create_table :organisations do |t|
      t.string :type
      t.string :name
      t.text :descr
      t.references :registrant, null: false, foreign_key: {to_table: :users}
      t.references :location, null: false, foreign_key: true
      t.boolean :public, null: false

      t.timestamps
    end

    create_table :memberships do |t|
      t.references :member, null: false, foreign_key: {to_table: :users}
      t.references :organisation, null: false, foreign_key: true
      t.integer :priv
    end
  end
end
