class CreateMapSources < ActiveRecord::Migration[7.2]
  def change
    create_table :map_sources do |t|
      t.string :name
      t.string :url
      t.boolean :is_vector
      t.string :attribution

      t.timestamps
    end
  end
end

