class CreateLocations < ActiveRecord::Migration[7.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.text :descr
      t.string :address
      t.string :ancestry, collation: 'C'
      t.string :breadcrumbs
      t.index 'ancestry'
      t.float :lat
      t.float :lng

      t.timestamps
    end
  end
end
