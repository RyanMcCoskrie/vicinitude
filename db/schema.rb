# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 12) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_logs", force: :cascade do |t|
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string "commentable_type", null: false
    t.bigint "commentable_id", null: false
    t.bigint "user_id", null: false
    t.datetime "time"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable"
    t.index ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "comments_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "comment_id", null: false
    t.index ["comment_id", "user_id"], name: "index_comments_users_on_comment_id_and_user_id"
    t.index ["user_id", "comment_id"], name: "index_comments_users_on_user_id_and_comment_id"
  end

  create_table "discussions", force: :cascade do |t|
    t.string "name"
    t.bigint "poster_id", null: false
    t.bigint "organisation_id", null: false
    t.datetime "time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organisation_id"], name: "index_discussions_on_organisation_id"
    t.index ["poster_id"], name: "index_discussions_on_poster_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.text "descr"
    t.bigint "user_id", null: false
    t.bigint "location_id", null: false
    t.bigint "organisation_id"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["location_id"], name: "index_events_on_location_id"
    t.index ["organisation_id"], name: "index_events_on_organisation_id"
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "events_locations", id: false, force: :cascade do |t|
    t.bigint "location_id", null: false
    t.bigint "event_id", null: false
    t.index ["event_id", "location_id"], name: "index_events_locations_on_event_id_and_location_id"
    t.index ["location_id", "event_id"], name: "index_events_locations_on_location_id_and_event_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "name"
    t.text "descr"
    t.string "address"
    t.string "ancestry", collation: "C"
    t.string "breadcrumbs"
    t.float "lat"
    t.float "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ancestry"], name: "index_locations_on_ancestry"
  end

  create_table "map_sources", force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.boolean "is_vector"
    t.string "attribution"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberships", force: :cascade do |t|
    t.bigint "member_id", null: false
    t.bigint "organisation_id", null: false
    t.integer "priv"
    t.index ["member_id"], name: "index_memberships_on_member_id"
    t.index ["organisation_id"], name: "index_memberships_on_organisation_id"
  end

  create_table "organisations", force: :cascade do |t|
    t.string "type"
    t.string "name"
    t.text "descr"
    t.bigint "registrant_id", null: false
    t.bigint "location_id", null: false
    t.boolean "public", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["location_id"], name: "index_organisations_on_location_id"
    t.index ["registrant_id"], name: "index_organisations_on_registrant_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string "subscribable_type", null: false
    t.bigint "subscribable_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscribable_type", "subscribable_id"], name: "index_subscriptions_on_subscribable"
    t.index ["subscribable_type", "subscribable_id"], name: "index_subscriptions_on_subscribable_type_and_subscribable_id"
    t.index ["user_id"], name: "index_subscriptions_on_user_id"
  end

  create_table "subscriptions_users", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "subscription_id", null: false
    t.index ["subscription_id", "user_id"], name: "index_subscriptions_users_on_subscription_id_and_user_id"
    t.index ["user_id", "subscription_id"], name: "index_subscriptions_users_on_user_id_and_subscription_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "user_name"
    t.string "real_name"
    t.string "email"
    t.string "password_hash"
    t.string "password_salt"
    t.integer "priv"
    t.string "verification_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "home_location_id"
    t.index ["home_location_id"], name: "index_users_on_home_location_id"
  end

  add_foreign_key "comments", "users"
  add_foreign_key "discussions", "organisations"
  add_foreign_key "discussions", "users", column: "poster_id"
  add_foreign_key "events", "locations"
  add_foreign_key "events", "organisations"
  add_foreign_key "events", "users"
  add_foreign_key "memberships", "organisations"
  add_foreign_key "memberships", "users", column: "member_id"
  add_foreign_key "organisations", "locations"
  add_foreign_key "organisations", "users", column: "registrant_id"
  add_foreign_key "subscriptions", "users"
  add_foreign_key "users", "locations", column: "home_location_id"
end
